\section{D'un point de vue graphique}


\subsection{Fonction croissante, décroissante, constante}


\begin{definition}[Intuitive]
\begin{itemize}
    \item On dit que $f$ est \textbf{croissante} sur un intervalle $I$ lorsque:\\ si $x$ augmente sur $I$ alors $f(x)$ augmente.
    \item On dit que $f$ est \textbf{décroissante} sur un intervalle $I$ lorsque:\\ si $x$ augmente sur $I$ alors $f(x)$ diminue.
\end{itemize}

$\phantom{1}$\hfill
\parbox{5cm}{
   Fonction croissante sur $I$: 
    
\begin{tikzpicture}[general, scale=0.7]
\draw[color=A4, fill=A4] (1,-0.2) rectangle (4,0.2);
\draw[color=A1] (3.5,-0.5) node[right] {{\boldmath$I$}};
\axeX{-1.2}{5}{-1,1,2,3,4}
\axeY{-1.2}{4}{-1,1,2,3}
\origine
\draw[tres epais,color=A1, smooth,samples=100,domain=1.0:4.0]
plot(\x,{0.35*((\x)-1)^2+0.5});
\draw [dotted, color=B1prime] (0,1.34)-- (2.55,1.34);
\draw [dotted, color=B1prime] (2.55,1.34)-- (2.55,0);
\draw [color=B1prime] (2.55,1.34)node {$\bullet$};
\draw [color=B1prime] (0,1.34)node {$\bullet$};
\draw [color=B1prime] (2.55,0)node {$\bullet$};
\draw[color=B1prime] (0,1.34) node[left] {{\boldmath $f(x)$}};
\draw[color=B1prime] (2.55,0) node[below] {{\boldmath $x$}};
\end{tikzpicture}
}
  \hfill\parbox{5cm}{  
    Fonction décroissante sur $I$:
    
\begin{tikzpicture}[general, scale=0.7]
\draw[color=A4, fill=A4] (1,-0.2) rectangle (4,0.2);
\draw[color=A1] (3.5,-0.5) node[right] {{\boldmath$I$}};
\axeX{-1.2}{5}{-1,1,2,3,4}
\axeY{-1.2}{4}{-1,1,2,3}
\origine
\draw[tres epais,color=A1, smooth,samples=100,domain=1.0:4.0]
plot(\x,{0-0.35*((\x)-1)^2+3.5});
\draw [dotted, color=B1prime] (0,2.66)-- (2.55,2.66);
\draw [dotted, color=B1prime] (2.55,2.66)-- (2.55,0);
\draw [color=B1prime] (2.55,2.66)node {$\bullet$};
\draw [color=B1prime] (0,2.66)node {$\bullet$};
\draw[color=B1prime] (0,2.66) node[left] {{\boldmath $f(x)$}};
\draw [color=B1prime] (2.55,0)node {$\bullet$};
\draw[color=B1prime] (2.55,0) node[below] {{\boldmath $x$}};
\end{tikzpicture}
}\hfill
$\phantom{1}$
\end{definition}



\begin{remarques}Soit $f$ une fonction et $\mathcal{C}_f$ sa courbe représentative dans un repère.\\ On voit sur un graphique que:
\begin{itemize} 
 \vspace{0.5em}
\parbox{0.6\textwidth}{
\item $f$ est croissante sur $I$ lorsque $\mathcal{C}_f$ \textcolor{H1prime}{\textbf{\og monte\fg}} sur $I$;
\item  $f$ est décroissante sur $I$ lorsque $\mathcal{C}_f$ \textcolor{H1prime}{\textbf{\og descend\fg}} sur $I$.
\item Lorsque sur un intervalle, la courbe est \textcolor{H1prime}{\textbf{horizontale}}, \\on dit
    que la fonction est \MotDefinition[constante (sens de variation)]{constante}{}.
On considère qu'elle est à la fois croissante et
    décroissante.
    \item Une fonction \textcolor{H1prime}{\textbf{qui ne change pas de sens de variations}} sur un intervalle est dite \MotDefinition{monotone}{} sur cet intervalle.
}\hfill\parbox{0.3\linewidth}{
\begin{tikzpicture}[general, scale=0.8]
\axeX{-1.35}{3.66} {-1,1,2,3}
\axeY{-1.22}{2.19}{-1,1}
\origine
\draw[tres epais,color=G1](1,1.3)--(3,1.3);
\draw [dotted, color=B1prime] (0,1.3)-- (2.55,1.3);
\draw [dotted, color=B1prime] (2.55,1.3)-- (2.55,0);
\draw  [color=B1prime] (2.55,1.3)node{$\bullet$};
\draw  [color=B1prime] (0,1.3)node{$\bullet$};
\draw[color=B1prime] (0,1.3) node[left] {{\boldmath$f(x)$}};
\draw  [color=B1prime] (2.55,0)node{$\bullet$};
\draw[color=B1prime] (2.55,0) node[below] {{\boldmath$x$}};
\end{tikzpicture}}
\end{itemize}
\end{remarques}


\subsection{Maximum et minimum d'une fonction}

\begin{definition}[Intuitive]
Sur un intervalle $I$,
\begin{itemize}\item  le \textbf{maximum} d'une fonction $f$ est la
plus grande des valeurs prises par
$f(x)$;
\item le \textbf{minimum} d'une fonction $f$ est la plus petite des
valeurs prises par
$f(x)$.
\end{itemize}
\vspace{1.5em}

\parbox{0.49\linewidth}{
Maximum en $x_0$

\begin{tikzpicture}[general, yscale=0.6]
\axeX{-1.25}{4.76}{-1,1,2,3,4}
\axeY{-1.22}{4.24}{-1,1,2,3}
\origine
\draw[tres epais, color=A1, smooth,samples=100,domain=1.0:4.0]
plot(\x,{0-((\x)-2.5)^2+3.5});
\draw [dotted, color=B1prime] (0,3.5)-- (2.49,3.5);
\draw [dotted, color=B1prime] (2.49,3.5)-- (2.49,0);
\draw  [color=B1prime] (2.49,3.5)node{$\bullet$};
\draw  [color=B1prime] (0,3.5)node{$\bullet$};
\draw[color=B1prime] (0,3.5) node[left] {{\boldmath$M$}};
\draw  [color=B1prime] (2.49,0)node{$\bullet$};
\draw[color=B1prime] (2.5,0) node[below] {{\boldmath$x_0$}};
\end{tikzpicture}
}\hfill\parbox{0.49\linewidth}{
Minimum en $x_0$

\begin{tikzpicture}[general, yscale=0.6]
\axeX{-1.23}{4.57}{-1,1,2,34}
\axeY{-1.23}{4.23}{-1,1,2,3}
\origine
\draw[tres epais, color=A1, smooth,samples=100,domain=1.0:4.0]
plot(\x,{0+((\x)-2.5)^2+0.5});
\draw [dotted, color=B1prime] (0,0.5)-- (2.49,0.5);
\draw [dotted, color=B1prime] (2.49,0.5)-- (2.49,0);
\draw  [color=B1prime] (2.49,0.5)node{$\bullet$};
\draw  [color=B1prime] (0,0.5)node{$\bullet$};
\draw[color=B1prime] (0,0.5) node[left] {{\boldmath$m$}};
\draw  [color=B1prime] (2.49,0)node{$\bullet$};
\draw[color=B1prime] (2.5,0) node[below] {{\boldmath$x_0$}};
\end{tikzpicture}
}
\end{definition}

\begin{definition}[Tableau de variations]
Un \MotDefinition{tableau de variations}{} regroupe toutes les informations concernant les variations d'une fonction sur son domaine de définition. 
\end{definition}

\begin{methode}[Dresser un tableau de variations \MethodeRefExercice*{2F3_E_dresser_tableau}]\label{2F3_M_dresser_tableau}
Un tableau de variations comporte deux lignes.
\begin{itemize}
 \item Aux extrémités de la 1\ieme\ ligne, on trouve les \textbf{bornes} du domaine de définition de la fonction. Entre les bornes, on place d'\textbf{éventuelles} valeurs particulières. 
\item Le sens de variation de la fonction est indiqué sur la 2\ieme\ ligne par \textbf{une ou plusieurs flèches} sur les intervalles où elle est monotone: $\nearrow$ pour croissante et $\searrow$ pour décroissante. 
\item Les valeurs pour lesquelles la fonction \textbf{n'est pas définie} sont indiquées par une double barre verticale sur la deuxième ligne.
\item On indique \textbf{au bout des flèches} les images des valeurs de la 1\iere\ ligne.
\end{itemize}

\exercice[0.45]

Dresser le tableau de variations de la fonction définie sur $[-2;2]$ par la courbe ci\nobreakdash-dessous.
\begin{center}
\begin{tikzpicture}[general, yscale=0.4]
\draw[quadrillageNIV2, xstep=0.5] (-2,-2) grid (2, 5);
\draw[quadrillage, ystep=2] (-2,-2) grid (2, 5);
\axeX{-2}{2}{-1,1}
\axeY{-2}{5}{2,4}
\origine
\draw[tres epais,color=B1prime, smooth,samples=100,domain=-2:2]
plot(\x,{(\x)^3-3*(\x)+1});
\end{tikzpicture}
\end{center}
\correction

\begin{tikzpicture}[general]
\tkzTabInit[espcl=1.5, lw=0.3mm]{$x$/1,$f(x)$/3}{$-2$,$-1$, 1, $2$}
\tkzTabVar{-/,+/3,-/$-1$,+/}
\end{tikzpicture}
\end{methode}


\section{D'un point de vue algébrique}

\subsection{Variations d'une fonction}
\begin{definition}[Croissance, décroissance sur un intervalle]
 Soit $f$ une fonction définie sur un intervalle $I$ et $x_1$ et $x_2$ deux nombres de $I$. 
\begin{itemize}
 \item Si $x_1\leqslant x_2$ implique $f(x_1) \leqslant f(x_2)$ alors $f$ est dite \MotDefinition[croissant (variations)]{croissante}{} sur $I$.
 \item Si $x_1\leqslant x_2$ implique $f(x_1) \leqslant f(x_2)$ alors $f$ est dite \MotDefinition[décroissant (variations)]{décroissante}{} sur $I$.
\end{itemize}
\parbox{0.49\textwidth}{
\begin{center}
    \begin{footnotesize}
$f$ est croissante sur $I$:\\
deux nombres de $I$ sont rangés \\dans le \textcolor{H1prime}{\textbf{même ordre}} que leurs images.\\
\end{footnotesize}
\begin{tikzpicture}[general,scale=0.8]
\draw[color=A4, fill=A4] (1,-0.2) rectangle (4,0.2);
\draw[color=A1] (3.5,-0.5) node[right] {{\boldmath$I$}};
\axeX{-1.2}{4.81}{-1,1,2,3,4}
\axeY{-1.31}{4}{1,2,3}
\origine
\draw[tres epais,color=A1, smooth,samples=100,domain=1.0:4.0]
plot(\x,{0.35*((\x)-1)^2+0.5});
\draw [dotted, color=B1prime] (0,0.63)-- (1.6,0.63);
\draw [dotted, color=B1prime] (1.6,0.63)-- (1.6,0);
\draw [dotted, color=B1prime] (0,2.54)-- (3.41,2.54);
\draw [dotted, color=B1prime] (3.41,2.54)-- (3.41,0);
\draw  [color=B1prime] (1.6,0.63)node{$\bullet$};
\draw  [color=B1prime] (0,0.63)node{$\bullet$};
\draw[color=B1prime] (0,0.63) node[left] {{\boldmath$f(x_1)$}};
\draw  [color=B1prime] (1.6,0)node{$\bullet$};
\draw[color=B1prime] (1.6,0) node[below] {{\boldmath$x_1$}};
\draw  [color=B1prime] (3.41,2.54)node{$\bullet$};
\draw  [color=B1prime] (0,2.54)node{$\bullet$};
\draw[color=B1prime] (0,2.54) node[left] {{\boldmath$f(x_2)$}};
\draw  [color=B1prime] (3.41,0)node{$\bullet$};
\draw[color=B1prime] (3.41,0) node[below] {{\boldmath$x_2$}};
\end{tikzpicture}
\end{center}\vspace{-1em}}\hfill\parbox{0.49\textwidth}{\begin{center}
\begin{footnotesize}
$f$ est décroissante sur $I$:\\
deux nombres de $I$ sont rangés \\dans l'\textcolor{H1prime}{\textbf{ordre inverse}} de leurs images.\\
\end{footnotesize}
\begin{tikzpicture}[general, scale=0.8]
\draw[color=A4, fill=A4] (1,-0.2) rectangle (4,0.2);
\draw[color=A1] (3.5,-0.5) node[right] {{\boldmath$I$}};
\axeX{-1.27}{4.72}{-1,1,2,3,4}
\axeY{-0.94}{4}{1,2,3}
\origine
\draw[tres epais,color=A1, smooth,samples=100,domain=1.0:4.0]
plot(\x,{0-0.35*((\x)-1)^2+3.5});
\draw [dotted, color=B1prime] (0,3.33)-- (1.7,3.33);
\draw [dotted, color=B1prime] (1.7,3.33)-- (1.7,0);
\draw [dotted, color=B1prime] (0,1.49)-- (3.4,1.49);
\draw [dotted, color=B1prime] (3.4,1.49)-- (3.4,0);
\draw  [color=B1prime] (1.7,3.33)node{$\bullet$};
\draw  [color=B1prime] (0,3.33)node{$\bullet$};
\draw[color=B1prime] (0,3.33) node[left] {{\boldmath$f(x_1)$}};
\draw  [color=B1prime] (1.7,0)node{$\bullet$};
\draw[color=B1prime] (1.7,0) node[below] {{\boldmath$x_1$}};
\draw  [color=B1prime] (3.4,1.49)node{$\bullet$};
\draw  [color=B1prime] (0,1.49)node{$\bullet$};
\draw[color=B1prime] (0,1.49) node[left] {{\boldmath$f(x_2)$}};
\draw  [color=B1prime] (3.4,0)node{$\bullet$};
\draw[color=B1prime] (3.4,0) node[below] {{\boldmath$x_2$}};
\end{tikzpicture}\end{center}\vspace{-1em}}
\end{definition}

%\begin{remarque}\\
% Pour une fonction \textcolor{H1prime}{\textbf{croissante}} sur $I$:\\ deux nombres de $I$ sont rangés dans le \textcolor{H1prime}{\textbf{même ordre}} que leurs images.
%
% 
% Pour une fonction \textcolor{H1prime}{\textbf{décroissante}} sur $I$:\\ deux nombres de $I$ sont rangés dans l'\textcolor{H1prime}{\textbf{ordre inverse}} de leurs images.
%\end{remarque}

\begin{propriete}[Tableau de variations des fonctions affines et de la fonction inverse]
Le sens de variation de la fonction affine dépend du signe de $a$.\\
La fonction inverse est décroissante sur $\mathbb{R^{-*}}$ et sur $\mathbb{R^{+*}}$.\\
\begin{tikzpicture}[general, yscale=0.8]
%\draw[color=J3, fill=J3] (0,0)--(5,5);
\tkzTabInit[lgt=1.3,espcl=1.7, lw=0.3mm, colorT=J3]{$x$/1,$ax+b$\\ avec \\$a>0$/2}{$-\infty$,$+\infty$}
\tkzTabVar{-/,+/}
\end{tikzpicture}
\begin{tikzpicture}[general, yscale=0.8]
\tkzTabInit[lgt=1.3,espcl=1.7, lw=0.3mm, colorT=J3]{$x$/1,$ax+b$\\ avec \\$a<0$/2}{$-\infty$,$+\infty$}
\tkzTabVar{+/,-/}
\end{tikzpicture}
\begin{tikzpicture}[general, yscale=0.8]
\tkzTabInit[lgt=1.3,espcl=1.5, lw=0.3mm, colorT=J3]{$x$/1,$\dfrac{1}{x}$/2}{$-\infty$,0,$+\infty$}
\tkzTabVar{+/,-D+/,-/}
\end{tikzpicture}
\end{propriete}

\begin{preuve}
\begin{itemize}
\item On considère une fonction $f$ tel que $f(x)=ax+b$ et deux nombres tels que $x_1<x_2$.\\ Si  $a<0$, $ax_1>ax_2$ et $f(x_1)>f(x_2)$. La fonction $f$ est donc décroissante sur $\mathbb{R}$.\\ 
Si  $a>0$, $ax_1<ax_2$ et $f(x_1)<f(x_2)$. La fonction $f$ est donc croissante sur $\mathbb{R}$. 
\item La preuve du sens de variation de la fonction inverse est l'objet de l'exercice \RefExercice{2F3_E_preuve_inverse}.
\end{itemize}
\end{preuve}
%\begin{exemple*1}
% Dresser les tableaux de variations des fonctions \textbf{affines} et de la fonction \textbf{inverse}.
%\correction
%
%\begin{tikzpicture}[general, yscale=0.8]
%\tkzTabInit[lgt=1.5,espcl=1.7]{$x$/1,$ax+b$\\ avec \\$a>0$/2}{$-\infty$,$+\infty$}
%\tkzTabVar{-/,+/}
%\end{tikzpicture}
%\begin{tikzpicture}[general, yscale=0.8]
%\tkzTabInit[lgt=1.5,espcl=1.7]{$x$/1,$ax+b$\\ avec \\$a<0$/2}{$-\infty$,$+\infty$}
%\tkzTabVar{+/,-/}
%\end{tikzpicture}
%\begin{tikzpicture}[general, yscale=0.8]
%\tkzTabInit[lgt=1,espcl=1.5]{$x$/1,$\dfrac{1}{x}$/2}{$-\infty$,0,$+\infty$}
%\tkzTabVar{+/,-D+/,-/}
%\end{tikzpicture}
%\end{exemple*1}


\subsection{Maximum et minimum d'une fonction}

\begin{definition}[Maximum, minimum et  extremum d'une fonction]
    \begin{itemize}
        \item Dire que $f$ admet un \MotDefinition{maximum}{}  en $a$ sur l'intervalle $I$
            signifie que :\\
            Il existe un réel $M$ tel que pour tout  $x$ dans $I$: $f(x)\leqslant M$ et $M=f(a)$.
        \item Dire que $f$ admet un \MotDefinition{minimum}{} en $b$ sur l'intervalle $I$
            signifie que :\\
            Il existe un réel $m$ tel que pour tout $x$ dans $I$: $f(x)\geqslant m$ et $m=f(b)$
\item Un \MotDefinition{extremum}{} est le terme générique pour désigner un maximum ou un minimum.
\end{itemize}
\parbox{0.49\textwidth}{   
\begin{tikzpicture}[general]
\axeX{-1.25}{4.73}{-1,1,2,3,4}
\axeY{-0.9}{3.24}{1,2,3}
\origine
\draw[epais, smooth,samples=100,domain=1.0:4.0, color=A1]
plot(\x,{0-((\x)-2.5)^2+2.5});
\draw [dotted] (0,2.5)-- (2.49,2.5);
\draw [dotted] (2.49,2.5)-- (2.49,0);
\draw [dotted] (0,1.59)-- (1.55,1.59);
\draw [dotted] (1.55,1.59)-- (1.55,0);
\draw  [color=B1prime] (2.49,2.5)node{$\bullet$};
\draw  [color=B1prime] (0,2.5)node{$\bullet$};
\draw[color=B1prime] (0,2.5) node[left] {{\boldmath$M$}};
\draw  [color=B1prime] (2.49,0)node{$\bullet$};
\draw[color=B1prime] (2.49,0) node[below] {{\boldmath$a$}};
\draw  [color=B1prime] (1.55,1.59)node{$\bullet$};
\draw  [color=B1prime] (0,1.59)node{$\bullet$};
\draw[color=B1prime] (0,1.59) node[left] {{\boldmath$f(x)$}};
\draw  [color=B1prime] (1.55,0)node{$\bullet$};
\draw[color=B1prime] (1.55,0) node[below] {{\boldmath$x$}};
\end{tikzpicture}}\hfill\parbox{0.49\textwidth}{
\begin{tikzpicture}[general]
\axeX{-1.25}{3.73}{-1,1,2,3,4}
\axeY{-0.9}{3.24}{1,2,3}
\origine
\draw[epais, smooth,samples=100,domain=1.0:4.0, color=A1]
plot(\x,{+((\x)-2.5)^2+0.5});
\draw [dotted] (0,0.5)-- (2.49,0.5);
\draw [dotted] (2.49,0.5)-- (2.49,0);
\draw [dotted] (0,1.49)-- (1.5,1.49);
\draw [dotted] (1.5,1.49)-- (1.5,0);
\draw  [color=B1prime] (2.49,0.5)node{$\bullet$};
\draw  [color=B1prime] (0,0.5)node{$\bullet$};
\draw[color=B1prime] (0,0.5) node[left] {{\boldmath$m$}};
\draw  [color=B1prime] (2.49,0)node{$\bullet$};
\draw[color=B1prime] (2.49,0) node[below] {{\boldmath$b$}};
\draw  [color=B1prime] (1.5,1.49)node{$\bullet$};
\draw  [color=B1prime] (0,1.49)node{$\bullet$};
\draw[color=B1prime] (0,1.49) node[left] {{\boldmath$f(x)$}};
\draw  [color=B1prime] (1.5,0)node{$\bullet$};
\draw[color=B1prime] (1.6,0) node[below] {{\boldmath$x$}};
\end{tikzpicture}}
\end{definition}

    
\begin{propriete}[Tableau de variations de la fonction carrée]
\begin{itemize}
 \item La fonction carrée est décroissante sur $\mathbb{R^{-}}$ et croissante sur $\mathbb{R^{+}}$.
\item Elle admet,  sur $\mathbb{R}$, un minimum en 0.
\end{itemize}
\begin{center}
\begin{tikzpicture}[general, xscale=1,yscale=0.8]
\tkzTabInit[lw=0.3mm, colorT=J3]{$x$/1,$x^2$/2}{$-\infty$,0,$+\infty$}
\tkzTabVar{+/,-/0,+/}
\end{tikzpicture}
\end{center}
\end{propriete}
%\begin{exemple}
% 
%Dresser le tableau de variations de la fonction carré.
%\correction
%
%\begin{tikzpicture}[xscale=0.7]
%\tkzTabInit{$x$/1,$x^2$/2}{$-\infty$,0,$+\infty$}
%\tkzTabVar{+/,-/0,+/}
%\end{tikzpicture}
%\end{exemple}

\begin{preuve}
 La preuve est l'objet de l'exercice \RefExercice{2F3_E_preuve_carre}.
\end{preuve}
