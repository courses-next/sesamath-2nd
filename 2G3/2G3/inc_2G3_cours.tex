
\section{Translations - Vecteurs associés}

\begin{definition}[Translation]On considère deux points $A$ et $B$ du plan. 

On appelle \MotDefinition{translation}{} \textbf{qui transforme {\boldmath$A$} en {\boldmath $B$}} la transformation qui, à tout point $M$ du plan, associe
l'unique point $M'$ tel que $[ AM' ]$ et $[ BM ]$ ont même milieu. 
\begin{center}
\begin{tikzpicture}[general, scale=0.4]
\draw [quadrillage] (0,0) grid (11,8);
\coordinate (A) at (1,1); 
\coordinate (B) at (3,5); 
\coordinate (M) at (7,2); 
\coordinate (N) at (9,6);
\coordinate (F) at (10.6, 7);
\draw [loosely dashed] (A)--(F);
\draw [loosely  dashed] (B)--(M); 
\draw [->, epais, color=A1] (A)--(B); 
\draw [->, epais, color=A1] (M)--(N); 
\pointC{A}{A}{below left}
\pointC{B}{B}{above left}
\pointC{M}{M}{below right}
\pointC{N}{M'}{above}
\draw (N) arc (32:52:3);
\draw (N) arc (32:12:3);
\draw (3,2.25) node[rotate=122, color=F1]  {{\boldmath $\approx$}};
\draw (7,4.75) node[rotate=122, color=F1]  {{\boldmath $\approx$}};
\draw (4,4.25) node[rotate=53, color=C1]  {{\boldmath $\infty$}};
\draw (6,2.75) node[rotate=53, color=C1]  {{\boldmath $\infty$}};
\draw (A) node{{\boldmath $\setminus$}};
\draw (M) node{{\boldmath $\setminus$}};
\end{tikzpicture}\hfill
\begin{tikzpicture}[general, scale=0.4]
\draw [quadrillage]  (0,0) grid (16,8);
\coordinate (D) at (1,1); 
\coordinate (A) at (2.2,1.48); 
\coordinate (B) at (5.8,2.92); 
\coordinate (M) at (8.5,4); 
\coordinate (N) at (12.1,5.44);
\coordinate (F) at (16, 7);
\draw [loosely dashed] (D)--(F);
\draw [->, epais, color=A1, line cap=butt] (A)--(B); 
\draw [->, epais, color=A1, line cap=butt] (M)--(N); 
\pointC{A}{A}{below left}
\pointC{B}{B}{above left}
\pointC{M}{M}{below right}
\pointC{N}{M'}{above}
\draw (N) arc (21.8:41.8:3);
\draw (N) arc (21.8:1.8:3);
\draw (A) node{{\boldmath $\setminus$}};
\draw (M) node{{\boldmath $\setminus$}};
\end{tikzpicture}
\end{center}
\end{definition}

\begin{vocabulaire}
\begin{itemize}
\item Le point $M'$ est appelé \textcolor{H1}{\textbf{image}} du point $M$.
\item On dit également que $M$ est le \textcolor{H1}{\textbf{translaté}} de $M'$.
\end{itemize}
\end{vocabulaire}

\begin{remarque}
Une \textbf{transformation} sert à \textbf{modéliser} mathématiquement un mouvement.
\begin{itemize}
\item La \textcolor{H1}{\textbf{symétrie centrale}} est la transformation qui modélise le demi-tour. 
\item La \textcolor{H1}{\textbf{translation}} est la transformation qui modélise le glissement rectiligne. Pour la définir, on indique la direction, le sens et la longueur du mouvement. 
\end{itemize}
\end{remarque}



\begin{propriete} On considère quatre points $A$,   $B$, $C$ et $D$. \\Dire que la translation qui transforme $A$ en $B$ transforme $C$ en $D$\\ équivaut à dire que  $ABDC$ est un \textbf{parallélogramme} (éventuellement aplati).  
\end{propriete}

\begin{preuve}
C'est la conséquence de la propriété: %la propriété caractéristique du parallélogramme: 
\og \textit{un quadrilatère est un parallélogramme \\si et seulement si ses diagonales se coupent en leur milieu}\fg.
\end{preuve}


\begin{definition}[Vecteurs associés]
\`A chaque translation est associé un \MotDefinition{vecteur}{}. \\Pour $A$ et $B$ deux points, le {\bfseries vecteur} $\vv{AB}$ est associé à la translation qui transforme $A$ en $B$. \\
La\textbf{ notation } \og vecteur $\vv{AB}$\fg\ regroupe les trois informations la définissant:\\ la direction (celle de la droite $(AB)$), le sens (de $A$ vers $B$) et la longueur $AB$. \\
$A$ est l'\MotDefinition[origine (vecteur)]{origine}{} du vecteur et $B$ son \MotDefinition[extrémité (vecteur)]{extrémité}{}.
\end{definition}


\begin{definition}
Deux vecteurs qui définissent la même translation sont dits \textbf{égaux}.\\
\parbox{0.50\textwidth}{
Deux vecteurs égaux ont 
\begin{itemize}
\item même direction; \item même sens; \item même longueur.
\end{itemize}
}\hfill\parbox{0.50\textwidth}{
\begin{center}
\begin{tikzpicture}[general, scale=0.75]
\draw [quadrillage, step=0.5] (-3,0.5) grid (4,3);
\draw [->] (-2,2) -- (1,1);
\draw [->] (-0,2.5) -- (3,1.5);
\draw [color=black] (-2,2) node {$/$};
\draw[color=black] (-2,2) node[above left] {$A$};
\draw[color=black] (1,1) node[below right] {$B$};
\draw[color=black] (-0,2.5)  node {$/$};
\draw[color=black] (-0,2.5) node[above left] {$C$};
\draw[color=black] (3,1.5) node[below right] {$D$};
\end{tikzpicture}
\end{center}
}
\end{definition}



\begin{propriete}
$\vv{AB}=\vv{CD}$ si et seulement si $ABDC$ est un parallélogramme (éventuellement aplati).\end{propriete}


\begin{methode} [Construire un vecteur \MethodeRefExercice*{2G3_E_construire_vecteur}] 
\exercice \label{2G3_M_construire_vecteur}

Placer trois points $A$, $B$ et $C$ non alignés. Construire le point $D$ tel que $\vv{CD}=\vv{AB}$. 

\correction
On construit le parallélogramme $ABDC$.\\
\begin{tikzpicture}[general, yscale=0.2,xscale=.5]
\draw[dashed] (-3,-1)-- (2,0);
\draw[->, epais] (-3,-1)-- (-1,3);
\draw[dashed] (-1,3)-- (4,4);
\draw[->, epais] (2,0)-- (4,4);
\draw (-3,-1) node[below] {$A$};
\draw (-1,3) node[above] {$B$};
\draw (2,0) node[below] {$C$};
\draw (4,4) node[right] {$D$};
\end{tikzpicture}
\end{methode}



\begin{remarque}
Une translation peut être définie par un point quelconque et son translaté.\\ Il existe donc une  \textbf{\textcolor{H1}{infinité}} de vecteurs associés à une translation. Ils sont tous égaux.  \\
Le vecteur choisi pour définir la translation est un \textbf{\textcolor{H1}{représentant}} de tous ces vecteurs. \\ La translation  \textbf{\textcolor{H1}{ne dépend pas}} du représentant choisi pour la définir. 
On le note souvent $\vv u$.
\end{remarque}

\begin{definition}[Vecteur nul]
Le vecteur associé à la translation qui transforme un point quelconque en lui-même\\ est le \MotDefinition{vecteur nul}{}, noté $\vv 0$. Ainsi, 
$\vv{AA} = \vv{BB}  = \vv{CC}= \ldots =\vv{0} $ 
\end{definition}

\begin{definition}[Vecteur opposé]
Le vecteur $\vv{BA}$ de la translation qui transforme $B$ en $A$ est appelé \MotDefinition{vecteur opposé}{} à $\vv{AB}$. 
\end{definition}
\begin{notation}
\begin{itemize}
\item Le vecteur opposé à $\vv{AB}$ se note $-\vv{AB}$ et on a l'égalité $\vv{BA}=-\vv{AB}$.
\item La notation $\overleftarrow{AB}$ n'existe pas.
\end{itemize}
\end{notation}

\begin{remarque} \par Deux vecteurs  \textbf{\textcolor{H1}{opposés}} ont même direction, même longueur mais sont de sens contraires.
\end{remarque}

\section{Opérations sur les vecteurs}

\subsection{Additions}

\begin{propriete}[Enchaînement de translations]
 L'enchaînement de deux translations est également une translation.
\end{propriete}


\begin{propriete}[Relation de Chasles]
 Soit $A$, $B$, $C$ trois points. 

 L'encha\^inement de la translation de vecteur $\vv{AB}$ puis de la translation de vecteur $\vv{BC}$ \\ est la translation de vecteur $\vv{AC}$ et on a: \MotDefinition[vecteurs (somme)]{$\vv{AB}+\vv{BC}=\vv{AC}$}{}.
\end{propriete}
\begin{remarque}
 $\vv{AB}+\vv{BA}=\vv{AA}=\vv 0$.
\end{remarque}


\begin{propriete}Soit $\vv {AB}$ et $\vv {CD}$ deux vecteurs. Alors : 
\begin{colitemize}{2}
\item  $\vv {AB} +\vv {CD}=\vv {CD} +\vv {AB}$ \item $\vv {AB} + \vv 0 = \vv 
{AB}$
\end{colitemize}
\vspace{-0.5em}
\end{propriete}

\begin{propriete}[Propriété du parallélogramme]
\parbox{0.49\textwidth}{
 Soit $A$, $B$, $C$, $D$ quatre points. 

Dire que $\vv{AD}=\vv{AB}+\vv{AC}$ \'equivaut \`a dire que $ABDC$ est un parall\'elogramme. 
}\hfill
\parbox{0.35\textwidth}{
\begin{tikzpicture}[general, scale=0.3]
\draw [quadrillage] (-1,-3) grid (9,5); 
\draw (1,2) node[left] {{\boldmath $\vv{AB}$}};
 \draw (5,3) node[above] {{\boldmath $\vv{AC}$}};
 \draw (4,2) node[below] {\rotatebox{-56.0}{{\boldmath $\vv{AB}+\vv{AC}$}}};
 \draw (0,0) node[below] {$B$};
 \draw (2,4) node[above] {$A$};
 \draw (8,2) node[above] {$C$};
 \draw (6,-2) node[right] {$D$};
\draw[<-, color=G1, epais] (0,0)--(2,4);
\draw[->, color=A1, epais] (2,4)--(6,-2);
\draw[->, color=C1, epais] (2,4)--(8,2);
\end{tikzpicture} 
}
\end{propriete}

\begin{preuve}
 
On suppose que $\vv{AD}=\vv{AB}+\vv{AC}$.

On utilise la relation de Chasles pour d\'ecomposer $\vv{AD}$: 
$\vv{AD}=\vv{AC}+\vv{CD}=\vv{AB}+\vv{AC}$.

On ajoute $\vv{CA}$ aux deux membres de l'\'egalit\'e :
$\underbrace{\vv{CA}+\vv{AC}}+\vv{CD}=\vv{AB}+\underbrace{\vv{AC}+\vv{CA}}$.

On utilise \`a nouveau la relation de Chasles avec $\vv{CA}+\vv{AC}=\vv 0$ et $\vv{AC}+\vv{CA}=\vv 0$.

Donc, la relation de d\'epart est \'equivalente \`a:
$\vv{CD}=\vv{AB}$ et $ABDC$ est un parall\'elogramme.

\end{preuve}


\begin{methode}[Construire la somme de deux vecteurs \MethodeRefExercice*{2G3_E_construire_somme}]\label{2G3_M_construire_somme}
On remplace l'un des deux vecteurs par un représentant: 
\begin{itemize}
\item soit de même origine afin d'utiliser la règle du parallélogramme;
\item soit d'origine l'extrémité de l'autre afin d'utiliser la relation de Chasles.
\end{itemize}
\exercice[0.3]
\vspace{-0.5em}
\begin{enumerate}
\item  Construire un carré $ABCD$ de centre $O$.
\item Construire les vecteurs \par
$\vv u=\vv{AB}+\vv{OD}$ et 

$\vv v=\vv{AD}+\vv{OC}$
\end{enumerate}
\correction
\hfill
\begin{tikzpicture}[general]
  \draw (0,0) node[below] {$A$};
  \draw (2,0) node[below] {$B$};
  \draw (2,2) node[above] {$C$};
  \draw (0,2) node[above] {$D$};
  \draw (1,1) node[right] {$O$};
  \draw[color=B1] (0.5,0.5) node[above] {{\boldmath $\vv u$}};
  \draw (1,-1) node {Avec la relation};
  \draw (1,-1.5) node {de Chasles};
  \draw (1,3) node[below, white] {$A$};
\draw (0,0)--(2,0)--(2,2)--(0,2)--cycle;
\draw (0,0)--(2,2);
 \draw[->, color=A1, epais] (0,0)--(2,0);
 \draw[->, color=A1, epais] (1,1)--(0,2);
 \draw[->, color=A1, epais, dashed, line cap=butt] (2,0)--(1,1);
 \draw[->, color=B1, epais, line cap=butt] (0,0)--(1,1);
\end{tikzpicture} 
\hfill
\begin{tikzpicture}[general]
  \draw (0,0) node[below] {$A$};
  \draw (2,0) node[below] {$B$};
  \draw (2,2) node[above] {$C$};
  \draw (0,2) node[above] {$D$};
  \draw (1,1) node[right] {$O$};
\draw (0,0)--(2,0)--(2,2)--(0,2)--cycle;
\draw[dashed] (1,1)--(1,3)--(0,2);
\draw (0,2)--(2,0);
\draw[->, color=A1, epais] (0,0)--(0,2);
\draw[->, color=A1, epais, line cap=butt] (1,1)--(2,2);
\draw[->, color=A1, epais, dashed] (0,0)--(1,1);
\draw[->, color=B1, epais] (0,0)--(1,3);
\draw[color=B1] (1,3) node[right] {$\vv v$};
\draw (1,-1) node {Avec la règle};
\draw (1,-1.5) node { du parallélogramme};
\end{tikzpicture} 
\end{methode}


\begin{remarque} 
$\vv{AB}+\vv{AC}=\vv 0$ si et seulement si $A$ est le milieu du segment $[BC]$.
\end{remarque}


\subsection{Soustraction}

\begin{definition} \MotDefinition{Soustraire un vecteur}{}, c'est additionner son oppos\'e.
\end{definition}

\begin{exemple}

Soit trois points $A$, $B$ et $C$ non align\'es. 

Donner un repr\'esentant du vecteur 

$\vv u =\vv{AB}-\vv{AC}$.

\correction

$\vv u=\vv{AB}-\vv{AC}$

$\vv u=\vv{AB}+\vv{CA}$

$\vv{u}=\vv{CA}+\vv{AB}$

$\vv{u}=\vv{CB}$ en utilisant la relation de Chasles.
\end{exemple}



\section{Coordonnées d'un vecteur}


\begin{definition}
\parbox{0.70\textwidth}{
Dans un rep\`ere $(O;I,J)$,  on considère la translation de vecteur $\vv u$ qui translate l'origine $O$ en un point $M$ de coordonnées $(a;b)$. 

Les \MotDefinition[coordonnées d'un vecteur]{coordonnées du vecteur}{} {\boldmath $\vv{u}$} sont les coordonn\'ees du point  $M$.  
On a $\vv u =\vv{OM}$ et on note $\vv u\left( \begin{array}{c} a\\b\end{array}\right)$.
}\hfill\parbox{0.25\textwidth}{
\begin{tikzpicture}[general, scale=0.3]
\draw [quadrillage] (-2,-2) grid (8,6);
\foreach \x/\y/\P/\N in {6/0/below/a,0/5/left/b} 
{\draw[color=C1] (\x,\y) node {\small  $+$};
\draw[color=C1]  (\x,\y) node[\P] {{\boldmath $\N$}};}
\draw[->,color=black] (-2,0) -- (8,0);
\draw[->,color=black] (0,-2) -- (0,6);
\origineO
\draw [dashed, color=F1, epais] (0,5) -- (6,5);
\draw [dashed, color=J1, epais] (6,0) -- (6,5);
\draw[color=black, color=A1] (3,3) node[left] {{\boldmath $\vv u$}};
\foreach \x/\y/\P/\N in {1/0/below/I,0/1/left/J,6/5/right/M} 
{\draw (\x,\y) node {\small  $+$};
\draw (\x,\y) node[\P] {$\N$};}
\draw [->, color=A1, tres epais, line cap=butt] (0,0) -- (6,5);
\end{tikzpicture}
}
\end{definition}


\begin{propriete}
 Deux vecteurs sont égaux si et seulement si ces vecteurs ont les mêmes coordonnées.
\end{propriete}


\begin{propriete}
Dans un repère $(O; I,J)$, les  coordonnées du vecteur $\vv{AB}$ sont $\left( \begin{array}{c} x_B-x_A\\y_B-y_A\end{array}\right)$.
\end{propriete}

\begin{preuve}
 Soit $A$, $B$ et $M$ de coordonnées respectives $(x_A; y_A)$, $(x_B; y_B)$ et $(x_M; y_M)$ dans un repère $(O; I, J)$ tels que $\vv{OM}=\vv{AB}$ et $OMBA$ est un parallélogramme.\\
Donc $[AM]$ et $[OB]$ ont même milieu.
 
    $\renewcommand{\arraystretch}{2}\left \lbrace \begin{array}{c}
                      \dfrac{x_A+x_M}{2}=\dfrac{x_B+x_O}{2}\\
                      \dfrac{y_A+y_M}{2}=\dfrac{y_B+y_O}{2}
                     \end{array} \right.\renewcommand{\arraystretch}{1}
$   soit  $\left \lbrace \begin{array}{c}
                     x_A+x_M=x_B\\
                    y_A+y_M=y_B
                     \end{array} \right.
$ soit $\left \lbrace \begin{array}{c}
                     x_M=x_B-x_A\\
                    y_M=y_B-y_A
                     \end{array} \right.
$

\end{preuve}

\begin{methode}[Lire les coordonnées d'un vecteur \MethodeRefExercice*{2G3_E_lire_coord}]
\exercice[0.55]\label{2G3_M_lire_coord}
  Lire les coordonnées du vecteur $\vv u$ sur la figure ci-dessous. 
  \vspace{-0.5em}
\begin{center}
\begin{tikzpicture}[general, yscale=0.6]
\foreach \a/\b/\c/\d in {0/0/5/0, 0/0.87/5/0.87, 0/2.61/5/2.61, 0/3.48/5/3.48, 0/1.74/1/3.48, 1/0/3/3.48, 2/0/4/3.48, 3/0/5/3.48, 4/0/5/1.74} {\draw[quadrillage] (\a,\b)--(\c, \d);}
\draw[axe] (0,0)--(2,3.48); 
\draw[axe] (0,1.74)--(5,1.74);
\coordinate (I) at (2,1.74); 
\coordinate (J) at (1.5,2.61); 
\draw (I) node[rotate=-25] {{\boldmath $/$}} node[below]{$I$};
\draw (J) node {{\boldmath $-$}} node[left]{$J$};
\draw[epais, ->, line cap=butt, color=F1] (0.5,2.61)--(3,0);
\draw (1.25,1.305) node[below] {{\boldmath $\vv{u}$}};
\end{tikzpicture}
\end{center}
 \correction
\begin{tikzpicture}[general, yscale=0.6]
\foreach \a/\b/\c/\d in {0/0/5/0, 0/0.87/5/0.87, 0/2.61/5/2.61, 0/3.48/5/3.48, 0/1.74/1/3.48, 1/0/3/3.48, 2/0/4/3.48, 3/0/5/3.48, 4/0/5/1.74} {\draw[quadrillage] (\a,\b)--(\c, \d);}
\draw[axe] (0,0)--(2,3.48); 
\draw[axe] (0,1.74)--(5,1.74);
\coordinate (I) at (2,1.74); 
\coordinate (J) at (1.5,2.61); 
\draw (I) node[rotate=-25] {{\boldmath $/$}} node[below]{$I$};
\draw (J) node {{\boldmath $-$}} node[above left]{$J$};
\draw[epais, ->, line cap=butt, color=F1] (0.5,2.61)--(3,0);
\draw (1.25,1.305) node[below, color=F1] {{\boldmath $\vv{u}$}};
\draw[epais, ->, line cap=butt, color=A1] (0.5,2.61)--(4.5,2.61);
\draw (2.5,2.61) node[above, color=A1] {{\boldmath $+4$}};
\draw[epais, ->, line cap=butt, color=A1] (4.5,2.61)--(3,0);
\draw (3.75,1.305) node[right, color=A1] {{\boldmath $-3$}};
\end{tikzpicture}

Les coordonnées de $\vv u$ sont $\left(\begin{array}{c}
4 \\ -3
\end{array}\right)
$.
\end{methode}

 
\begin{methode}[Construire un vecteur à partir de ses coordonnées\MethodeRefExercice*{2G3_E_construire_coord_vecteur}]
\exercice\label{2G3_M_construire_coord_vecteur}
 Dans un repère orthonormé, construire le représentant d'origine $A(6; 2)$ du vecteur $\vv u$ de coordonnées $\left( \begin{array}{c} -4 \\3\end{array}\right)$.   
 
\correction

\begin{tikzpicture}[general, scale=0.4]
\draw [quadrillage] (-2,-2) grid (8,6);
\axeOI{-2}{8}
\axeOJ{-2}{6}
\origineO
\draw [<-, color=A1, tres epais] (2,5) -- (6,2);
\draw [<-,dashed, color=F1, epais] (2,2) -- (6,2);
\draw [<-,dashed, color=J1, epais] (2,5) -- (2,2);
\draw[color=black, color=F1] (4,2) node[below] {{\boldmath $-4$}};
\draw[color=black, color=J1] (2,3.5) node[left] {{\boldmath $+3$}};
\draw[color=black, color=A1] (4.5,4) node {{\boldmath $\vv u$}};
\pointGraphique{6}{2}{A}{below right}
\end{tikzpicture}
\end{methode}



\begin{methode}[Repérer un point défini par une égalité vectorielle\MethodeRefExercice*{2G3_E_reperer_point_egalite}]
\exercice[0.25]\label{2G3_M_reperer_point_egalite}
 Dans un repère orthogonal $(O;I,J)$, on a les points $A(-2;3)$, $B(4;-1)$ et $C(5;3)$. \\Calculer les coordonnées 
 \begin{enumerate}
  \item du vecteur $\vv{AB}$;
  \item du point $D$ tel que $\vv{AB}=\vv{CD}$.
 \end{enumerate}
\correction
\vspace{-2em}
 \begin{enumerate}
  \item Les coordonnées du  vecteur $\vv{AB}$ sont 
$\left( \begin{array}{c}
                    x_B-x_A \\ y_B-y_A
                   \end{array}
\right)$ soit $\left( \begin{array}{c}
                    4-(-2) \\ -1-3
                   \end{array}
\right)$.
Donc $\vv{AB}$ a pour coordonnées $\left( \begin{array}{c}
                    6 \\ -4
                   \end{array}
\right)$.

\item On cherche $(x_D;y_D)$, les coordonnées du 
point $D$ tel que $\vv{AB}=\vv{CD}$.\\ 
Or, \og  \textit{ si deux vecteurs sont égaux 
alors ils ont mêmes coordonnées}\fg. \\
Donc le couple $(x_D;y_D)$ est la solution du système: 


$\left\lbrace \begin{array}{c}
                    x_D-x_C=x_B-x_A \\ y_D-y_C=y_B-y_A
                   \end{array}\right.$ 
soit  $\left \lbrace \begin{array}{l}
                                                        x_D-5=6 \\y_D-3=-4
                                                       \end{array}
\right.$
soit  $\left \lbrace \begin{array}{l}
                                                        x_D=6+5=11 \\y_D=-4+3=-1
                                                       \end{array}
\right.$
\vspace{0.1em}
\par Les coordonnées du point $D$ sont $(11;-1)$.
 \end{enumerate}
\end{methode}



\begin{propriete}[Somme de deux vecteurs]
Si $\vv{u}$ et $\vv{v}$ sont deux vecteurs de coordonn\'ees respectives $\left (\begin{array}{c}
x\\y
\end{array}\right)$ et 
$\left(\begin{array}{c}
x'\\y'
\end{array}\right)$,

alors les coordonn\'ees du vecteur \MotDefinition[vecteurs (somme)]{$\vv{u}+\vv{v}$}{} sont 
$\left(\begin{array}{c}
x+x'\\y+y'
\end{array}\right)$.
\end{propriete}

\begin{methode}[Rep\'erer un point d\'efini par une somme vectorielle\MethodeRefExercice*{2G3_E_repere_point_somme}]
\exercice[0.25]\label{2G3_M_repere_point_somme}
Dans un rep\`ere orthogonal $(O; I, J)$, on place les points $A(2;3)$, $B(4;-1)$, $C(5;3)$ et $D(-2;-1)$.
Quelles sont les coordonn\'ees du point $E$ tel que $\vv{AE}=\vv{AD}+\vv{CB}$?
\correction
On cherche les coordonn\'ees $(x_E;y_E)$ du point $E$ tel que  $\vv{AE}=\vv{AD}+\vv{CB}$. \\
Donc le couple $(x_E;y_E)$ est solution du syst\`eme:

$\left\lbrace\begin{array}{l}
x_E-x_A=(x_D-x_A)+(x_B-x_C)\\y_E-y_A=(y_D-y_A)+(y_B-y_C)
\end{array}\right.$
soit 
$\left \lbrace \begin{array}{l}
x_E-2=(-2-2)+(4-5)\\y_E-3=(-1-3)+(-1-3)
\end{array}
\right.$ 
soit 
$\left \lbrace \begin{array}{l}
x_E-2=-5 \\y_E-3=-8
\end{array}
\right.$ 
soit 
$\left \lbrace \begin{array}{l}
x_E=-5+2=-3 \\y_E=-8+3=-5
\end{array}
\right.$
\par
Les coordonn\'ees du point $E$ sont $(-3;-5)$.
\end{methode}




\section{Multiplication par un réel}


\begin{definition}
Soit $\vv{u}$ un vecteur de coordonn\'ees $(x;y)$ et $\lambda$ un r\'eel. La \MotDefinition[vecteur (produit par un réel)]{multiplication de {\boldmath $\vv{u}$} par {\boldmath $\lambda$}}{} est \\le vecteur $\lambda\vv{u}$ de coordonn\'ees $(\lambda x;\lambda y)$.
\end{definition}

\begin{methode}[Rep\'erer le produit d'un vecteur par un réel\MethodeRefExercice*{2G3_E_reperer_produit}]
\exercice[0.25]\label{2G3_M_reperer_produit}
 Dans un rep\`ere orthogonal, construire le repr\'esentant d'origine $A(1; 4)$ du vecteur $-0,5 \vv u$ avec $\vv u \left( \begin{array}{c} 2 \\-3\end{array}\right)$.   
\correction\parbox{0.49\linewidth}{
$\vv u$ a pour coordonn\'ees $\left( \begin{array}{c} 2 \\-3\end{array}\right)$. 
Donc $-0,5 \vv u$ a pour coordonn\'ees $\left( \begin{array}{c} -0,5\times 2 \\-0,5\times (-3)\end{array}\right)$  
soit $\left( \begin{array}{c} -1 \\1,5\end{array}\right)$. 
}\hfill\parbox{0.49\linewidth}{
\begin{tikzpicture}[general, yscale=0.3, xscale=1.2]
\draw [quadrillage, xstep=0.25] (-0.5,-1) grid (3.25,7);
\axeX{-0.5}{3.25}{1}
\axeY{-1}{7}{1}
\draw [->, color=A1,epais] (1,4) -- (3,1);
\draw [->, color=C1,epais] (1,4) -- (0,5.5);
\draw [->,dashed, color=A1, epais](1,4) -- (1,5.5);
\draw [->,dashed, color=C1, epais] (1,5.5)--(0,5.5);
\draw[color=C1] (0.5,5.5) node[above] {{\boldmath $-1$}};
\draw[color=A1] (1,4.75) node[right] {{\boldmath $+1,5$}};
\draw[color=C1] (0.5,4) node[left] {{\boldmath $-0,5\vv u$}};
\draw[color=A1] (2,3) node[right] {{\boldmath $\vv u$}};
\pointGraphique{1}{4}{A}{below}
\end{tikzpicture}}
\end{methode}

\begin{propriete}
Soient deux vecteurs $\vv{AB}$ et $\vv{CD}$ et $\lambda$ un r\'eel tels que  $\vv{AB}=\lambda\vv{CD}$. 
\begin{itemize}
 \item si $\lambda>0$, $\vv{AB}$ et $\vv{CD}$ sont de m\^eme sens et  $AB=\lambda CD$.
\item si $\lambda<0$, $\vv{AB}$ et $\vv{CD}$ sont de sens contraires et  $AB=-\lambda CD$.
\end{itemize}
\end{propriete}

\begin{remarque}
 $\vv u$ et $\lambda \vv u$ ont la m\^eme direction.  Leurs sens et leurs longueurs d\'ependent de $\lambda$.
\end{remarque}


\section{Colin\'earit\'e}


\begin{definition}
On dit que deux vecteurs non nuls sont \MotDefinition{colinéaires}{} \\ si et seulement si leurs coordonn\'ees dans un m\^eme rep\`ere sont proportionnelles. 
\end{definition}

 \begin{remarque}
Par convention, le vecteur nul est colin\'eaire \`a tout vecteur $\vv{u}$. %En effet, $\vv{0}=0.\vv{u}$.
 \end{remarque}



\begin{propriete}
Deux vecteurs $\vv{u}$ et $\vv{v}$ non nuls sont colin\'eaires lorsqu'il existe un r\'eel $\lambda$ tel que $\vv{v}=\lambda\vv{u}$.
\end{propriete}
 

 
\begin{methode}[V\'erifier la colin\'earit\'e de deux vecteurs\MethodeRefExercice*{2G3_E_colinearite}]\label{2G3_M_colinearite}
Pour v\'erifier que  deux vecteurs non nuls $\vv{u}\left (\begin{array}{c}
x\\y
\end{array}\right)$ et $\vv{v}\left(\begin{array}{c}
x'\\y'
\end{array}\right)$  sont colin\'eaires, il suffit de:
\begin{description}
\item[possibilité 1] trouver un r\'eel $\lambda$ non nul tel que $x'=\lambda x$ et $y'=\lambda y$; 
\item[possibilité 2] vérifier que les produits en croix, $xy'$  et $x'y$, sont égaux.
\end{description}
\exercice
Soit $(O; I, J)$ un rep\`ere orthogonal. Les vecteurs suivants sont-ils colin\'eaires? \BotStrut
\begin{enumerate}
\item $\vv u \left( \begin{array}{c}
2\\6 
\end{array}
\right)$ et $\vv v \left( \begin{array}{c}
-6 \\ -18
\end{array}
\right)$. \BotStrut
\item $\vv w \left( \begin{array}{c}
-5 \\ 3\end{array}\right)$ et $\vv z \left( \begin{array}{c}12 \\ -7\end{array}\right)$.
\end{enumerate}
\correction
\begin{enumerate}
\item $-6=-3\times 2$ et $-18=-3\times 6$ donc $\vv v=-3\vv u$.
  
 $\vv u$ et $\vv v$ sont donc colin\'eaires.
\item $-5\times(-7)=35$ et $3\times 12=36$. 
  
  Les produits en croix ne sont pas \'egaux.  
  
  Donc $\vv w$ et $\vv z$ ne sont pas colin\'eaires.
\end{enumerate}
\end{methode}


\begin{propriete}
\begin{itemize}
\item Deux droites $(AB)$ et $(CD)$ sont \textbf{parall\`eles}\\ si et seulement si les vecteurs $\vv{AB}$ et $\vv{CD}$ sont colin\'eaires;
\item Trois points $A$, $B$ et $C$ sont \textbf{align\'es} si et seulement si les vecteurs $\vv{AB}$ et $\vv{AC}$ sont colin\'eaires.
\end{itemize}
\vspace{-0.5em}
\end{propriete}

\begin{exemple}[0.3]
$A(1;2 )$; $B(3;1 )$ et $C(5; 3)$ \\ sont-ils alignés?
\correction 
 On calcule les coordonnées des vecteurs$\vv{AB}$ et $\vv{AC}$ puis les produits en croix:
$(x_B-x_A)(y_C-y_A)=(3-1)(3-2)=2\times 1=2$
et ainsi: 
$(y_B-y_A)(x_C-x_A)=(1-2)(5-1)=-1\times4=-4$

 
Les coordonnées des vecteurs $\vv{AB}$ et $\vv{AC}$ ne sont pas proportionnelles. 

Donc $A$, $B$, $C$ ne sont pas alignés.
\end{exemple}

