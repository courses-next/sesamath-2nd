Sésamath, manuel Seconde
=========================

Un mirroir des source complètes du manuel Sésamath Seconde.

# Licence
Based on the works of [Sésamath](http://sesamath.net). The original source files are available at [their site](http://manuel.sesamath.net/index.php?page=telechargement_2nde_2014).

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0).
