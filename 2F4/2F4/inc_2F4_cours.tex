\section{Signe d'une fonction affine}
\begin{propriete}
 
Soit $a$ et $b$ deux nombres réels avec $a\neq 0$.

La \MotDefinition{fonction affine}{} définie sur $\mathbb{R}$ par $f(x)=ax+b$ s'annule et change de signe une fois dans son domaine de définition pour $x=-\dfrac{b}{a}$.


\parbox{0.49\linewidth}{Si $a>0$, elle est négative puis positive.

\begin{tikzpicture}[general]
\draw[fill=F4, color=F4] (-2,-1) rectangle (-0.5,2);
\draw[fill=G4, color=G4] (4,-1) rectangle (-0.5,2);
 \draw[quadrillage55] (-2,-1)grid(4,2);
 \axeX{-2}{4}{1}
 \axeY{-1}{2}{1}
 \origine
\draw[epais, color=F1] (-2,-1)--( -0.5,0);
\draw[epais, color=G1] (-0.5,0)--( 2.5,2);
\draw[epais, color=F1] (-1.25,0.75) node[draw, circle] {$-$};
\draw[epais, color=G1] (2.75,0.75) node[draw, circle] {$+$};
\end{tikzpicture}
}\hfill\parbox{0.49\linewidth}{
Si $a<0$, elle est positive puis négative.

\begin{tikzpicture}[general]
\draw[fill=G4, color=G4] (-2,-1) rectangle (1.5,2);
\draw[fill=F4, color=F4] (4,-1) rectangle (1.5,2);
 \draw[quadrillage55] (-2,-1)grid(4,2);
 \axeX{-2}{4}{1}
 \axeY{-1}{2}{1}
 \origine
\draw[epais, color=G1] (-1.5,2)--(1.5,0);
\draw[epais, color=F1] (1.5,0)--( 3,-1);
\draw[epais, color=F1] (3.25,0.75) node[draw, circle] {$-$};
\draw[epais, color=G1] (-1.25,0.75) node[draw, circle] {$+$};
\end{tikzpicture}
}

\end{propriete}


\begin{preuve}
 Soit $f$ une fonction affine définie sur $\mathbb{R}$ par $f(x)=ax+b$ avec $a\neq 0$.

$f(x)=0$ implique $ax+b=0$ soit $ax=-b$ et $x=-\dfrac{b}{ a}$. 

%Donc l'antécédent de 0 par $f$ est $-\dfrac{b}{\BotStrut a}$. 

%\begin{multicols}{2}
\begin{description}
\item[Si]{\boldmath $a>0$}, la fonction $f$ est croissante. 
\begin{itemize}
\item Pour $x<-\dfrac{b}{a}$,  $f(x)<f\left(-\dfrac{\TopStrut b}{\BotStrut a}\right)$. Or, $f\left(-\dfrac{b}{a}\right)=0$ donc $f(x)<0$.
\item Pour $x>-\dfrac{b}{a}$,  $f(x)>f\left(-\dfrac{ b}{a}\right)$. Or, $f\left(-\dfrac{b}{a}\right)=0$ donc $f(x)>0$. 
\end{itemize}
Donc $f$ est négative sur $] -\infty ; -b/a[$ puis positive sur $]-b/a; +\infty[$.
\item[Si]{\boldmath $a<0$}, la fonction $f$ est décroissante. 
\begin{itemize}
\item Pour $x<-\dfrac{b}{a}$,  $f(x)>f\left(-\dfrac{\TopStrut b}{\BotStrut a}\right)$. Or, $f\left(-\dfrac{b}{a}\right)=0$ donc  $f(x)>0$.
\item Pour $x>-\dfrac{b}{a}$,  $f(x)<f\left(-\dfrac{ b}{\BotStrut a}\right)$. Or, $f\left(-\dfrac{b}{a}\right)=0$ donc $f(x)<0$.
\end{itemize}
Donc $f$ est positive sur $] -\infty ; -b/a[$  puis négative $]-b/a; +\infty[$.
\end{description}
%\end{multicols}
\end{preuve}

\begin{methode}[Dresser le tableau de signes d'une fonction affine \MethodeRefExercice*{2F4_E_signe_affine}]\label{2F4_M_signe_affine}
Le tableau de signes d'une fonction affine comporte deux lignes. 

Sur la \textbf{première ligne} on indique les bornes du domaine de définition de la fonction et\\  la valeur qui annule la fonction. 

Sur la \textbf{deuxième ligne}, par des pointillés verticaux sous la valeur qui annule, \\on crée deux cases  dans lesquelles on indique le signe de la fonction.

 
%\begin{tikzpicture}[scale=0.85]\tkzTabInit{$x$/1,$ax+b$\\avec\\$a>0$/2}{$-\infty$,$-\dfrac{b}{a}$,$+\infty$}\tkzTabLine{ , - , z , +, }\end{tikzpicture}\hfill
%\begin{tikzpicture}[scale=0.85]\tkzTabInit{$x$/1,$ax+b$\\avec\\$a<0$/2}{$-\infty$,$-\dfrac{b}{a}$,$+\infty$}\tkzTabLine{ , + , z , -, }\end{tikzpicture}


%\begin{multicols}{2}
\exercice[0.3]
Dresser le tableau de signes de la fonction $g$ définie sur $\mathbb{R}$ par $g:x\mapsto -3x+4$.

\correction
Le coefficient directeur,$-3$,  est négatif donc $g$ est décroissante.

Recherche de la valeur qui annule: 

$-3x+4=0$ soit $x=\dfrac{\TopStrut -4}{\BotStrut -3}$ soit $x=\dfrac{4}{3}$. 

\begin{tikzpicture}[scale=0.7]
\tkzTabInit[ color, colorC=H4, colorV=H4, colorL=H4, colorT=A4, lw=0.3mm]{$x$/1,$-3x+4$/1}{$-\infty$,$\frac{ 4}{ 3}$,$+\infty$}
\tkzTabLine{ , + , z , -, }
\end{tikzpicture}

\end{methode}


\section{Factorisation}
\begin{remarque}
 En classe de seconde, on a déjà des outils pour factoriser une grande partie des polynômes de degré 2. D'autres outils seront étudiés en Première. En Terminale, dans certaines séries, toutes les expressions seront factorisables.
\end{remarque}


\begin{methode}[Factoriser une expression littérale \MethodeRefExercice*{2F4_E_factoriser}]\label{2F4_M_factoriser}
Soit $a$, $b$, $k$ trois nombres réels. 
\begin{itemize}
 \item Si un facteur est \textbf{apparent}, on utilise: $ka+kb=k(a+b)$.
 \item Si un facteur \textbf{n'est pas apparent}, on utilise les identités remarquables:

$a^2-b^2=(a+b)(a-b)$
\hfill
$a^2-2ab+b^2=(a-b)^2$
\hfill
 $a^2+2ab+b^2=(a+b)^2$
\end{itemize}
\exercice
 Factoriser les expressions suivantes: 
\begin{enumerate}
\item $4ac-6ab$
\item $(x-2)(5x-1)+(2x+7)(x-2)$
\item $x^2-6x+9$
\item $36x^2-81$
\end{enumerate}
\correction
\begin{enumerate}
\item $4ac-6ab=2a(2c-3b)$
\item $(x-2)(5x-1)+(2x+7)(x-2)$

$=(x-2)\left((5x-1)+(2x+7)\right)$

$=(x-2)(7x+6)$
\item $x^2-6x+9=x^2-2\times x\times 3+3^2=(x-3)^2$
\item $36x^2-81=(6x)^2-9^2=(6x+9)(6x-9)$
\end{enumerate}
\end{methode}


\section{Signe du produit  de deux fonctions affines}

\begin{methode*2}[\'Etudier le signe du produit de deux fonctions affines\MethodeRefExercice*{2F4_E_produit}]\label{2F4_M_produit}
Pour déterminer le signe du produit de deux fonctions affines, on construit un tableau de signes à 4 lignes.
\begin{enumerate}
\item La \textbf{1\ieme\ ligne} indique les bornes de l'ensemble de définition \\et les valeurs qui annulent le produit des deux fonctions affines.
\item Les \textbf{2\ieme\ et 3\ieme\ lignes} indiquent le signe de chacune des deux fonctions affines.
\item La \textbf{4\ieme\ ligne} se remplit avec la règle des signes du produit de deux nombres relatifs:
\begin{enumerate}
\item des facteurs de même signe donnent un produit positif;
\item des facteurs de signes contraires donnent un produit négatif.
\end{enumerate}
\end{enumerate}


\exercice 

Résoudre l'inéquation $(3x+4)(-2x+6)\leqslant 0$.
\correction

On étudie le signe de la fonction $h$ définie sur $\mathbb{R}$ par
$h(x)=(3x+4)(-2x+6)$. 

Recherche des valeurs qui annulent: 
\begin{itemize}
\item $3x+4=0$ implique $x=-\dfrac{\TopStrut 4}{\BotStrut 3}$. 
\item $-2x+6=0$ implique $x=3$. 
%\item $-\dfrac{\TopStrut 4}{\BotStrut 3}<3$
\end{itemize}

\begin{tikzpicture}[general, yscale=0.8]
\tkzTabInit[espcl=1.5, color, colorC=H4, colorV=H4, colorL=H4, colorT=A4, lw=0.3mm]{$x$/1,$3x+4$/1, $-2x+6$/1, $h(x)$/1}{$-\infty$,$-\dfrac{\TopStrut 4}{\BotStrut 3}$,3,$+\infty$}
\tkzTabLine{ , - , z ,  +, t,+ }
\tkzTabLine{ ,  + ,t,+, z ,  -  }
\tkzTabLine{ , -,z,+,z,- }
\end{tikzpicture}

Les solutions de cette inéquation %$(3x-4)(-2x+6)~\leqslant~0$ 
sont les nombres de l'ensemble $\left] -\infty; -\dfrac{4}{3}\right]\cup\left[3; +\infty\right[$.

\end{methode*2}

\section{Signe d'une fonction homographique}


 \begin{definition}[Fonction homographique]

% Soit $a$, $b$, $c$, $d$ quatre réels tels que $ad-bc\ne 0 et c\ne0$.
On appelle \MotDefinition{fonction homographique}{} toute fonction $h$  qui peut s'écrire comme quotient de fonctions affines. 
Soit $a$, $b$, $c$, $d$ quatre réels tels que $ad-bc\neq 0$ et $c\neq0$: $h(x)=\dfrac{\TopStrut ax+b}{\BotStrut cx+d}$.
\end{definition}

\begin{propriete}
\begin{multicols}{2}
Une fonction homographique est définie sur $\mathbb{R}$ privé de la valeur qui annule son dénominateur dite \og \MotDefinition{valeur interdite}{}\fg. 

Sa \textbf{courbe représentative} est une hyperbole qui comporte deux branches disjointes. 

$\phantom{1}$\hfill\begin{tikzpicture}[general, scale=0.3]
 \draw[quadrillage, xstep=2, ystep=2] (-8,-5)grid(10,5);
 \axeX{-8}{10}{2}
 \axeY{-5}{5}{2}
 \origine
\draw[epais, color=A1, smooth, domain=-10:-0.181, variable=\x, samples=100] plot (2+\x,0.5+1/\x);
\draw[epais, color=A1, smooth, domain=0.222:8, variable=\x, samples=100] plot (2+\x,0.5+1/\x);
\end{tikzpicture}
\end{multicols}
\end{propriete}

\begin{methode}[Donner le domaine de définition d'une fonction homographique \MethodeRefExercice*{2F4_E_def_homographique}]
Pour identifier ce domaine de définition, il suffit de trouver la valeur interdite.

\exercice \label{2F4_M_def_homographique}
Quel est le domaine de définition de la fonction $f$ définie par $f(x)=\dfrac{5x+4}{3x-7}$?

\correction

Recherche de la valeur interdite: $3x-7=0 \Leftrightarrow x=\dfrac{7}{3}$

Le domaine de définition de la fonction $f$ définie par 

$f(x)=\dfrac{5x+4}{3x-7}$ est $\mathbb{R}\setminus \left\lbrace \dfrac{7}{3}\right\rbrace$.
\end{methode}



\begin{methode*2}[Donner le tableau de signes d'une fonction homographique \MethodeRefExercice*{2F4_E_quotient}]
 La méthode est similaire à celle du produit de deux fonctions affines. 

La valeur qui \textbf{annule le dénominateur} ne faisant pas partie du domaine de définition de la fonction  doit être indiquée par \textbf{ une double barre}. 


\exercice \label{2F4_M_quotient}

Résoudre l'inéquation $\dfrac{\TopStrut 3x-5}{2x+7}> 0$.

\correction

 On étudie le signe de la fonction $l$ définie par $l(x)=\dfrac{3x-5}{\BotStrut 2x+7}$. 
\begin{itemize}
\item Recherche de la valeur interdite:\\ $2x+7\neq 0$ implique $x\neq -\dfrac{\TopStrut 7}{2}$. \\ Donc $l$ est définie sur $\mathbb{R}\setminus\left\lbrace -\dfrac{7}{2}\right\rbrace$.
\item Recherche de la valeur qui annule $l$: \\$3x-5=0$ implique $x= \dfrac{5}{3}$.
\item Comparaison des valeurs trouvées pour les ranger sur la 1\iere\ ligne du tableau:$-\dfrac{7}{2}<\dfrac{5}{3}$.
\end{itemize}
%\vspace{-1em}
\begin{tikzpicture}[general,yscale=0.8]
\tkzTabInit[espcl=1.5, color, colorC=H4, colorV=H4, colorL=H4, colorT=A4, lw=0.3mm]{$x$/1,$3x-5$/1, $2x+7$/1, $l(x)$/1}{$-\infty$,$-\dfrac{ 7}{ 2}$,$\dfrac{5}{3}$,$+\infty$}
\tkzTabLine{ ,  - ,t,-, z ,  +  }
\tkzTabLine{ , - , z ,  +, t,+ }
\tkzTabLine{ , +,d,-,z,+ }
\end{tikzpicture}


Les solutions de l'inéquation $\dfrac{\TopStrut 3x-5}{\BotStrut 2x+7}> 0$ sont les nombres de l'ensemble $\left] -\infty; -\dfrac{7}{2}\right[\cup\left]\dfrac{\TopStrut 5}{\BotStrut 3}; +\infty\right[$.


\end{methode*2}
