
%%%%%%%%%%%%%%%% I page 10 et 11
\section{Série statistique. Tableau d'effectifs. Fréquences d'apparition.}

Lorsqu'on étudie un certain \textbf{caractère} sur une population donnée, on relève une valeur du caractère par individu. L'ensemble des données obtenues (ou toutes les valeurs prises par le caractère) constitue les \MotDefinition{données brutes}{}. \\
Le caractère peut être {\bfseries qualitatif} (couleur d'une voiture)  ou {\bfseries quantitatif} (taille d'un individu). \\
Les données brutes comportent souvent des valeurs qui se répètent.


\begin{definition}[Série statistique]
Lors d'un relevé de mesures effectué sur les individus d'une population,\\ l'ensemble des données collectées constitue une \MotDefinition{série statistique}{}.
\begin{itemize}
 \item Une série statistique à caractère quantitatif est dite \MotDefinition[ordonnée (serie)]{ordonnée}{} \\après que les valeurs collectées ont été rangées dans l'ordre croissant (ou décroissant). 
 \item L'\MotDefinition{étendue}{} désigne l'écart entre la plus grande et la plus petite des valeurs\\ prises par le caractère.
\end{itemize}
\end{definition}


\begin{definition}[Modalités]
 Les différentes valeurs possibles pour un caractère s'appellent les \MotDefinition{modalités}{} du caractère. 
\end{definition}

\begin{remarque}
Dans ce qui suit, l'entier $p$ désigne le \textcolor{H1}{\bfseries nombre de modalités.} 
\begin{itemize}
 \item Si $p$ est \textcolor{H1}{\bfseries petit}, les modalités, désignées par $x_1$, $x_2$, ..., $x_p$, sont rangées dans l'ordre croissant dans la première ligne du tableau.
Les effectifs correspondants, désignés par $n_1$, $n_2$, ..., $n_p$, sont placés sur la deuxième ligne du tableau. 
\begin{center}
  \begin{tableau}[lc]{0.7\linewidth}{5}\hline
 Caractère & $x_1$ & $x_2$ & ... & $x_p$ \\\hline
 Effectif & $n_1$ & $n_2$ & ... & $n_p$ \\\hline
  \end{tableau}
\end{center}
\item Si $p$ est \textcolor{H1}{\bfseries grand}, le recensement de  toutes les modalités du caractère rendrait le tableau trop grand et par conséquent illisible. 
Dans ce cas, la première ligne du tableau présente des intervalles contigus appelés \MotDefinition[classe d'une série statistique ]{classes}{} et les effectifs sur la deuxième ligne comptent le nombre de fois où le caractère a pris une valeur dans la classe correspondante. 
\begin{center}
 \begin{tableau}[lc]{0.7\linewidth}{5}\hline
Caractère & $[a_1;a_2[$ & $[a_2;a_3[$ & ... & $[a_p;a_{p+1}]$ \\\hline
Effectif & $n_1$ & $n_2$ & ... & $n_p$ \\\hline
 \end{tableau}
\end{center}
\end{itemize}
\end{remarque}


\begin{exemple*1}
On a demandé aux 35~élèves de la 2\ieme Z du Lycée \'Evariste Galois la durée de leur trajet pour se rendre au lycée le matin et voici le résultat du sondage:

\umin{10}; \umin{6}~\us{17}; \umin{21}; \umin{44}; $\dfrac{3}{10}$\uh{}; \umin{9}; \umin{28}; \umin{40}; $\dfrac{1}{4}$\uh{}; environ \umin{40}; presque \umin{30}; \umin{7}; $\dfrac{1}{\BotStrut 5}$\uh{}; \umin{23}; \umin{4}; \umin{21}; \umin{39}; \umin{28}; \umin{11}; \umin{35}; \umin{22}; \umin{19}; \umin{35}; \umin{26}; \us{10} (j'habite au lycée, ma mère est l'adjointe du proviseur); \umin{19}; trop longtemps; \umin{21}; \umin{11}; \umin{6}; \umin{32}; \umin{18}; ça dépend du vent; \umin{27}; une demi-heure.

Dresser un tableau d'effectifs de cette série statistique et calculer l'étendue. 
\correction Voici une répartition de la durée des trajets maison-lycée des élèves de la 2\ieme Z du Lycée \'Evariste Galois:

\parbox{0.49\linewidth}{
\begin{tableau}[LC]{\linewidth}{4}{>{\centering}m{2.2cm}}\hline
 Durée du trajet & $[0;15[$ & $[15;30[$ & $[30;45]$\\\hline
 Effectifs & 10 & 15 & 8\\\hline
\end{tableau}}\hfill\parbox{0.39\linewidth}{Par lecture du tableau: $45-0=45$.

L'étendue est de \umin{45}.}
\end{exemple*1}

\begin{notation}
\begin{itemize}
\item La notation $[a;b[$ indique l'ensemble des nombres compris entre $a$ et $b$, $a$ inclus et $b$ exclu.
\item 
Les données brutes égales à $a_2 ; a_3 ; \ldots ; a_p$ ne doivent pas être comptabilisées deux fois.\\ 
Ici, les données égales à $a_2$ sont comptabilisées dans la classe $[a_2; a_3[$  et non pas dans la classe $[a_1; a_2[$. 
\end{itemize}
\end{notation}

\begin{definition}[Fréquence d'apparition]
On considère une série statistique comportant $p$ modalités (ou $p$ classes) d'effectifs $n_1$,..., $n_p$ et d'effectif total $N$.
La \MotDefinition{fréquence d'apparition}{} de la modalité (ou de la classe) correspond à la proportion d'individus dont le caractère est égal à cette modalité (ou appartenant à cette classe). 
Ainsi, pour tout entier $i$ compris entre 1 et $p$:\\
\vspace{-2em}
\begin{center}
\begin{tabular}[\linewidth]{ccc}

 $f_i=\dfrac{\TopStrut n_i}{N}$
& et &$f_1~+~f_2~+~\ldots~+~f_p=1$
\end{tabular}
\end{center}
\vspace{-0.5em}
\end{definition}

\begin{preuve}
$f_1~+~f_2~+~\ldots~+~f_p=\dfrac{n_1}{N}~+~\dfrac{n_2}{N}~+~\ldots~+~\dfrac{n_p}{N}=\dfrac{n_1~+~n_2~+~\ldots~+~n_p}{N}=\dfrac{N}{N} =1. $
\end{preuve}

\begin{exemple*1}

\parbox{0.39\linewidth}{Le tableau ci-contre indique  la répartition du nombre d'enfants de moins de 25~ans dans les familles des Bouches-du-Rhône en 1999 et 2009. 

Construire un diagramme en barres comparatif de 1999 et 2009.}
\hfill\parbox{0.59\linewidth}{
\begin{tableau}[LC]{\linewidth}{3}{m{4cm}}\hline
 Nombre de familles avec&  en 2009 &  en 1999\\\hline
 Aucun enfant& \nombre{244918}&\nombre{220109}\\\hline
 1 enfant&\nombre{131271}&\nombre{124597}\\\hline
 2 enfants&\nombre{109776}&\nombre{102135}\\\hline
 3 enfants&\nombre{35907}&\nombre{35708}\\\hline
 4 enfants ou plus&\nombre{13311}&\nombre{14564}\\\hline
Ensemble&\nombre{535183}&\nombre{497113}\\\hline
\end{tableau}
\vspace{-1.5em}
\begin{center}\textit{\footnotesize{Source :  Insee, RP1999 et RP2009 exploitations complémentaires.}}\end{center}}
 \correction
 
\parbox{0.49\linewidth}{\begin{tableau}[LC]{\linewidth}{3}{m{2.5cm}}\hline
Fréquences en \upc{} des familles avec & en 2009 & en 1999\\\hline
 Aucun enfant&45,76&44,28\\\hline
 1 enfant&24,53&25,06\\\hline
 2 enfants&20,51&20,55\\\hline
 3 enfants&6,71&7,18\\\hline
 4 enfants ou plus&2,49&2,93\\\hline
Ensemble&100&100\\\hline
\end{tableau}
 Ce tableau peut se remplir avec un tableur.}
 \hfill\parbox{0.49\linewidth}{
 \begin{tikzpicture}[general, yscale=0.08]
 \draw[quadrillageNIV2, ystep=2, xstep=6](0,0) grid(6,50);
 \draw[quadrillage, ystep=10, xstep=6](0,0) grid(6,50);
 \axeY{0}{50}{0, 10, 20, 30,  40} 
 \draw[axe] (0,0)--(6,0);
\draw(-0.5,25) node[above, rotate=90]{Fréquences en \%};
\foreach \x/\y in {0/45.76,1/24.53,2/20.51,3/6.71,4/2.49} 
{\draw[fill=A3] (0.6+\x,0) rectangle (1+\x,\y); 
\draw (1+\x,0) node [below] {$\x$};}
\foreach \x/\y in {0/44.28,1/25.06,2/20.55,3/7.18,4/2.93} 
{\draw[fill=B3] (1.4+\x,0) rectangle (1+\x,\y); }
 \draw (4.7,48) node[right]  {2009};
 \draw (4.7,42) node[right]  {1999};
 \draw[fill=A3] (4.5,47) rectangle (4.7,48);
 \draw[fill=B3] (4.5,41) rectangle (4.7,42);
\end{tikzpicture}
\vspace{-2em}
\begin{center}
\textit{{Répartition des enfants dans les familles des Bouches-du-Rhône en 1999 et 2009}}
\end{center}
\vspace{-2em}}
\end{exemple*1}


%%%%%%% II page 11 12 13
\section{Médiane et quartiles}

Dans cette partie et la suivante, on considère uniquement des séries statistiques à caractère quantitatif. 
\begin{definition}[Médiane]
Dans une série statistique \textbf{ordonnée}:\\
 une \MotDefinition{médiane}{} partage les valeurs prises par le caractère en deux groupes de même effectif. 
\end{definition}


\begin{methode}[Déterminer une médiane \MethodeRefExercice*{2SP1_E_mediane}]
Si les valeurs sont peu nombreuses, il est plus simple d'ordonner la série \\et de \textit{séparer les valeurs prises par le caractère en deux groupes} de même effectif.

{\bf Si l'effectif total est impair}, une valeur restera entre les deux demi-groupes. 

Cette valeur sera une médiane.

{\bf Si l'effectif total est pair}, n'importe quelle valeur comprise entre la dernière valeur \\du premier groupe  et la première valeur du second groupe peut être considérée comme la médiane. Le plus souvent, la moyenne de ces deux valeurs est choisie comme médiane. 

\exercice[0.4]\label{2SP1_M_mediane}
Les élèves de 2\ieme\ Z du Lycée E. Galois participent à une chorale. 
Le chef de ch\oe ur souhaite partager les élèves en deux lignes suivant leur taille. 
Aider le chef de ch\oe ur  en déterminant leur taille médiane.

172; 162; 190; 190; 169; 164; 177; 181; 189; 161; 164; 182; 185; 188; 169; 190; 193; 189; 179; 180; 173; 193; 166; 164; 163; 164; 190; 176; 176; 192; 173; 194.

\correction
Voici la liste ordonnée des tailles des élèves de 2\ieme Z.

161; 162; 163; 164; 164; 164; 164; 166; 169; 169; 172; 173; 173; 176; 176; \textbf{177}; \textbf{179}; 180; 181; 192; 185; 188; 189; 189; 190; 190; 190; 190; 192; 193; 193; 194.

L'effectif total de cette série est 32. 
Une fois la série ordonnée, les 16\ieme\  et 17\ieme\ valeurs partagent cette série en deux groupes de 16 élèves.
Leur moyenne est $\dfrac{\TopStrut 177~+~179}{\BotStrut 2}$ donc une valeur possible de la médiane est \ucm{178}. \\
Le chef de ch\oe ur peut partager la classe en deux lignes: ceux mesurant moins de \ucm{178} et ceux mesurant plus de \ucm{178}.
\end{methode}


\begin{definition}[Quartiles]
Le \MotDefinition{premier quartile}{} d'une série statistique numérique est la plus petite valeur prise\\ par le caractère telle qu'au moins \upc{25} des valeurs lui soient inférieures ou égales. 

Le \MotDefinition{troisième quartile}{} d'une série statistique numérique est la plus petite valeur prise\\ par le caractère telle qu'au moins \upc{75} des valeurs lui soient inférieures ou égales.
\end{definition}

\begin{methode*1}[Déterminer les quartiles à partir des fréquences  \MethodeRefExercice*{2SP1_E_quartile}]
\exercice\label{2SP1_M_quartile}
Reprendre la répartition du nombre d'enfants de moins de 25~ans dans les familles des Bouches-du-Rhône en 2009 et calculer une médiane puis les quartiles de la série.
\correction 
On utilise la colonne des cumuls.

\parbox{0.4\textwidth}{
\begin{itemize}
 \item Les \upc{25} de la population sont\\ atteints pour les familles sans enfant.

Donc le 1\ier\ quartile est  0~enfant.

\item Les \upc{50} de la population  sont\\ atteints pour les familles avec un enfant ou moins.
Donc la médiane est  1~enfant.

\item Les \upc{75} de la population  sont\\ atteints pour les familles avec deux enfants. 
Donc le 3\ieme\ quartile est  2~enfants.
\end{itemize}}\hfill
\parbox{0.48\textwidth}{
\begin{tableau}[lc]{\linewidth}{3}\hline
Fréquences en \upc{} des familles avec & en 2009  & cumuls\\\hline
 Aucun enfant&45,76&45,76\\\hline
 1 enfant&24,53&70,29\\\hline
 2 enfants&20,51&90,80\\\hline
 3 enfants&6,71&97,51\\\hline
 4 enfants ou plus&2,49&100\\\hline
Ensemble&100&\cellcolor{FondTableaux}\\\hline
\end{tableau}}
\end{methode*1}


\begin{remarque}
 Pour une série regroupée par classes, les valeurs brutes prises par le caractère ne sont pas accessibles. 
Il est possible d'obtenir une approximation d'une médiane et des quartiles par lecture graphique sur  le polygone des fréquences cumulées croissantes. 

\end{remarque}

\begin{exemple}

Déterminer graphiquement une médiane et les quartiles de la série constituée par les tailles des exploitations agricoles professionnelles en 2005 en Franche-Comté. 

    \begin{tableau}[LC]{\linewidth}{2}{m{4cm}}
    \hline
    &Effectifs\\\hline
    Moins de \uha{5}&370\\\hline
    De 5 à moins de \uha{20}&190\\\hline
    De 20 à moins de \uha{50}&840\\\hline
    De 50 à moins de \uha{75}&1720\\\hline
    De 75 à moins de \uha{100}&1380\\\hline
    De 100 à moins de \uha{200}&1880\\\hline
    \uha{200} et plus&400\\\hline
    \textbf{Ensemble}&\textbf{6780}\\\hline
    \end{tableau}\textit{\footnotesize{ Source Insee: Enquête structure des exploitations 2005 }}
    
    
   \begin{tableau}[LC]{\linewidth}{2}{m{4cm}}
    \hline
     & F.C.C. en \upc{}\\\hline
    Moins de \uha{5}&5,5\\\hline
    De 5 à moins de \uha{20}&8,3\\\hline
    De 20 à moins de \uha{50}&20,6\\\hline
    De 50 à moins de \uha{75}&46\\\hline
    De 75 à moins de \uha{100}&66,4\\\hline
    De 100 à moins de \uha{200}&94,1\\\hline
    \uha{200} et plus&100\\\hline
    \end{tableau}
\correction

    
    \begin{center}
    \textit{Polygone des fréquences cumulées croissantes des tailles des exploitations agricoles de la région Franche-Comté en 2006.}
\end{center}

\newcommand{\taillesexploitationsagricolesFC}{(0,0)(5,5.5)(20,8.3)(50,20.6)(75,46)(100,66.4)(200,94.1)}
\begin{tikzpicture}[general, xscale=0.03, yscale=0.09]
\draw[quadrillageNIV2, xstep=20, ystep=5](0,0) grid(200,100);
\draw[quadrillage, xstep=20, ystep=25](0,0) grid(200,100);
\axeY{0}{100}{25,50,75}
\axeX{0}{210}{0,20,40,60,80,100,120,140,160,180}
\draw(0,90) node[left]{en \upc{}};
\draw(210,0) node[below]{en \uha{}};
\draw[tres epais, J1] plot coordinates \taillesexploitationsagricolesFC;

\draw[A1, dashed, epais, ->](0,25)--(54.5,25);
\draw[A1, dashed, epais, ->](0,50)--(80,50);
\draw[A1, dashed, epais, ->](0,75)--(130,75);
\draw[A1, dashed, epais, <-](54.5,0)--(54.5,25);
\draw[A1, dashed, epais, <-](80,0)--(80,50);
\draw[A1, dashed, epais, <-](130,0)--(130,75);
\end{tikzpicture}


Par lecture graphique, on lit que: 


\begin{colitemize}{2}
\item le 1\ier\ quartile est \uha{55};
\item une médiane est \uha{80};
\item le 3\ieme\ quartile est \uha{130}.
\end{colitemize}
\end{exemple}

%%%%% III p 13 et 14
\section{Moyenne}


\begin{definition}[Moyenne]
La \MotDefinition{moyenne}{} d'une série statistique se note $\overline{x}$.
$$\overline{x}=\dfrac{\mbox{somme totale des valeurs prises par le caractère}}{\mbox{nombre de valeurs}}$$
Si $x_1, x_2, \ldots, x_p$ désignent les $p$ modalités du caractère d'une série statistique et\\ $n_1, n_2, \ldots, n_p$ désignent les effectifs correspondants, alors $$\overline{x}=\dfrac{n_1~\times~x_1~+~n_2~\times~x_2~+~n_3~\times~x_3~+~\cdots~+~n_p~\times~x_p}{n_1+~n_2~+~n_3~+~\ldots+n_p}$$
\end{definition}


\begin{propriete}
Si $x_1, x_2, \ldots, x_p$ désignent les $p$ modalités du caractère d'une série statistique, et\\ $f_1, f_2, \ldots, f_p$ désignent les fréquences correspondantes alors,
\vspace{-0.5em}
$$\overline{x}=f_1~\times~x_1~+~f_2~\times~x_2~+~f_3~\times~x_3~+~\cdots~+~f_p~\times~x_p$$
\end{propriete}
\begin{preuve}
Posons $N=n_1~+~n_2~+~n_3~+~\ldots~+~n_p$

$\renewcommand{\arraystretch}{1.5}\begin{array}{rcl}

 \overline{x}&=&\dfrac{\TopStrut n_1~\times~x_1~+~n_2~\times~x_2~+~n_3~\times~x_3~+~\cdots~+~n_p~\times~x_p}{\BotStrut n_1~+~n_2~+~n_3~+~\cdots~+~n_p}\\
 \overline{x}&=&\dfrac{n_1~\times~x_1}{N}~+~\dfrac{n_2~\times~x_2}{N}~+~\dfrac{n_3~\times~x_3}{N}~+~\cdots~+~\dfrac{n_p~\times~x_p}{N}
\\
 \overline{x}&=&f_1~\times~x_1~+~f_2~\times~x_2~+~f_3~\times~x_3~+~\cdots~+~f_p~\times~x_p
  \end{array}\renewcommand{\arraystretch}{1}$
\end{preuve}
\begin{methode*1}[Calculer une moyenne à partir des fréquences \MethodeRefExercice*{2SP1_E_moyenne}] 
 \exercice \label{2SP1_M_moyenne} Calculer le salaire net annuel moyen en France en 2005.
\begin{tableau}[lc]{\linewidth}{3}
\hline
Régions  &Fréquences (en \%) &Salaires (en euros)\\\hline
Région Parisienne & 25,3&\u{29237} \\\hline
Bassin Parisien & 15,7 & \u{20318} \\\hline
Nord & 5,8 & \u{20501} \\\hline
Est & 8 & \u{20946} \\\hline
Ouest & 12,1 & \u{19891}  \\\hline
Sud-Ouest & 9,3 & \u{20542} \\\hline
Centre-Est & 11,9 & \u{25811} \\\hline
Méditerranée & 10 & \u{20993} \\\hline
DOM & 1,8 & \u{20495}   \\\hline
\end{tableau}
\begin{center}
\textit{\footnotesize{ Source: DADS (exploitation  au 1/12 en  2005), Insee}}
\end{center}
\vspace{-1em}
\correction
 $\overline{x}=f_1~\times~x_1~+~f_2~\times~x_2~+~f_3~\times~x_3~+~\cdots~+~f_p~\times~x_p$

Ici, après avoir exprimé les fréquences sous forme décimale: 

$\begin{array}{rcl}
  \overline{x}&=& 0,253 \times 29~237~+~0,157 \times 20~318~+~0,058 \times 20~501~+~
 0,08 \times 20~946~+~0,121 \times 19~891\\&& +0,093 \times 20~542~+~0,119 \times 25~811~+~0,1 \times 20~993~+~0,018 \times 20~495=23~308,6\\
     \end{array}
$

Le salaire annuel net moyen en France en 2005 était d'environ~\ueuro{23308}.
\end{methode*1}



\begin{remarque}
Pour une \textcolor{H1}{\bfseries série triée en classes},  la répartition à l'intérieur d'une classe est souvent considérée comme \textcolor{H1}{\bfseries homogène.} La valeur prise par le caractère est supposée unique et égale au \MotDefinition[centre d'une classe]{centre de la classe}{}. 
Le centre $c$ de la classe $[a; b[$ vaut: $c=(a+b)\div {2}$.
\end{remarque}

\begin{exemple}[0.49]
Déterminer l'âge moyen d'un demandeur d'emploi dans les Bouches-du-Rhône en 2009.
 \begin{tableau}[LC]{\linewidth}{2}{>{\centering}m{2cm}}\hline
Tranche d'âge  &Nombre de demandeurs\\ \hline
  $[15;25[$ &24~146
 \\\hline
$[25;55[$ &107~761
 \\\hline
$[55;65]$ &29~441
 \\\hline
 \end{tableau} 
 \vspace{-1em}
 \begin{center} \textit{\scriptsize{ Source: INSEE, RP2009 exploitations principales}}\end{center}\vspace{-1,5em}
\correction
\vspace{-1,5em}
\begin{itemize}
\item Le centre de la classe $[15;25[$ est 20;
\item celui de la classe $[25;55[$ est 40;
\item celui de la classe $[55;65[$ est 60.
\end{itemize}
\vspace{-1em}
$\renewcommand{\arraystretch}{1.5}\begin{array}{rcl}
\overline{x}&=&\dfrac{24146 \times 20+ 107761 \times 40+ 29441 \times 60}{ 24146+107761+29441}
 \end{array}\renewcommand{\arraystretch}{1}$

soit $\overline{x}\approx 40,66$

L'âge moyen d'un demandeur d'emploi dans les Bouches-du-Rhône en 2009 était d'environ 41~ans.
\end{exemple}

 
