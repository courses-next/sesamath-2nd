%% 1p82
\section{Définitions}
\begin{definition}[Fonction]
  Soit $\mathcal{D}$ un ensemble de nombres réels. Définir une \MotDefinition{fonction}{} $f$ sur
  $\mathcal{D}$ revient à associer, à chaque réel $x$ de
  $\mathcal{D}$, un réel \emph{et un seul}, appelé
  \MotDefinition{image}{} de $x$. 
\end{definition}

\begin{vocabulaire}
 $\mathcal{D}$ est l'\MotDefinition[ensemble de définition]{ensemble de définition de $f$}{}. $\mathcal{D}$ peut être l'ensemble des nombres réels, noté $\mathbb{R}$, ou être constitué d'une ou plusieurs parties de $\mathbb{R}$.   
\end{vocabulaire}

\begin{notations}
  \begin{itemize}
  \item Soit $a \in \mathcal{D}$. L'\textcolor{H1}{\textbf{image}} du nombre $a$ par la
    fonction $f$ est \textcolor{H1}{\textbf{unique}} et se note $f(a)$.\\
    $f(a)$ se lit  \og $f$ de $a$ \fg{}.  La notation suivante se rencontre également
    $f: a \mapsto f(a)$.
    \item Si $b$ est l'image de $a$, on a l'égalité $f(a)=b$ et $a$ est un \textcolor{H1}{\textbf{antécédent}} de $b$ par la fonction $f$.
  \end{itemize}
\end{notations}
 


\begin{definition}[Fonctions de référence]
Une \MotDefinition{fonction de référence}{} est une fonction simple qui permet l'étude d'une famille plus large de fonctions.  
  \begin{itemize}
     \item La \MotDefinition{fonction carrée}{} est la fonction qui à $x$ associe $x^2$. 
     
     Elle est définie sur $\mathbb{R}$.

     \item La \MotDefinition{fonction inverse}{} est la fonction qui à $x$ associe $\dfrac{1}{x}$.
     
      Elle est définie sur $\mathbb{R}^*$.
  
      \item Une \MotDefinition{fonction affine}{} est une fonction qui à $x$ associe $mx+p$ (avec $m$ et $p$ réels). 
      
      Elle est définie sur $\mathbb{R}$.
  \end{itemize}
 
\end{definition}

\begin{notations}
  \begin{itemize}
\item Quand un intervalle contient des nombres aussi grands (aussi petits) que l'on veut, \\le symbole $+\infty$ ($-\infty$) remplace la borne.
  \item $\mathbb{R}^+$ note l'ensemble des nombres réels positifs ou
    nuls. C'est l'ensemble $\left[0;+\infty\right[$.
  \item $\mathbb{R}^-$ note l'ensemble des nombres réels négatifs
    ou nuls. C'est l'ensemble $\left]-\infty;0\right]$.
  \item $\mathbb{R}^*$ ou $\mathbb{R}\setminus \{0\}$ note l'ensemble des nombres non nuls.
  C'est l'ensemble $\left]-\infty;0\right[ \cup \left]0; +\infty\right[$.
  \end{itemize}
\end{notations}

%2 p82
\section{Différentes représentations d'une fonction}

\begin{definition}[Expression d'une fonction]
  Soit $f$ une fonction, $\mathcal{D}$ son ensemble de définition et
  $x \in \mathcal{D}$. 
  L'\MotDefinition{expression algébrique}{} d'une fonction donne directement $f(x)$ en fonction de la variable $x$.
\end{definition}

\begin{exemple}
Une fonction est déterminée par le programme de calcul  suivant:
\begin{itemize}
\item choisir un nombre;
\item lui ôter 6;
\item prendre le carré du résultat.
\end{itemize}
Trouver l'expression définissant cette fonction.

\correction

On note $g$ la fonction qui à un nombre $x$, lui associe le résultat
du programme de calcul.

Après avoir choisi un nombre $x$, le programme lui ôte 6.

 On obtient donc $x-6$.

Ensuite le programme élève ce nombre au carré soit : $(x-6)^2$.

Donc la fonction liée à ce programme de calcul est définie par :
$g : x \mapsto (x-6)^2$.
\end{exemple}



\begin{definition}[Tableau de valeurs]
  Soit $f$ une fonction, $\mathcal{D}$ son ensemble de définition et $x$ un élément de $\mathcal{D}$.

  Un \MotDefinition{tableau de valeurs}{} d'une
  fonction $f$ donne, sur la première ligne (ou colonne), différentes
  valeurs de la variable $x$ et, en vis-à-vis sur la deuxième ligne
  (ou colonne), les images $f(x)$ qui leur sont associées.
\end{definition}

\begin{remarque}
  Un tableau de valeurs n'est pas unique. \\Il dépend du choix des valeurs de $x$ sur la première ligne
  (ou colonne).
\end{remarque}
% 
\begin{methode}[Construire un tableau de valeurs \MethodeRefExercice*{2F1_E_tableau_valeurs}] %

  \exercice[0.3]\label{2F1_M_tableau_valeurs}

  Dresser un tableau de 10~valeurs de la fonction $g$ définie par
  $g(x)=(2x-3)^2$ à partir de $x=-5$ avec un pas de $1$.

  \correction

  Le pas de 1 signifie qu'il y a une différence de 1 entre chaque valeur de $x$ de la première ligne.

  \begin{tableau}{\linewidth}{11}
    \hline
    $x$ & $-5$ & $-4$ & $-3$ & $-2$ & $-1$ & $0$ & $1$ & $2$ & $3$ & $4$
    \\\hline
    $f(x)$ & 169 & 121 & 81 & 49 & 25 & 9 & 1 & 1 & 9 & 25
    \\\hline
  \end{tableau}
\end{methode}

\begin{definition}[Courbe représentative d'une fonction]
  La \MotDefinition[courbe représentative]{courbe représentative de la fonction $f$}{} dans un repère
  est l'ensemble des points de coordonnées $(x;f(x))$ où $x$ parcourt
  le domaine de définition $\mathcal{D}$ de la fonction $f$. 
 
 Elle est souvent notée $\mathcal{C}_f$.

  L'\MotDefinition[équation de courbe]{équation de cette courbe représentative}{} est : $y=f(x)$.
\end{definition}

\begin{vocabulaire}
La courbe représentative de la fonction carrée s'appelle une
  \MotDefinition{parabole}{},\\ celle de la fonction inverse une \MotDefinition{hyperbole}{}.
\end{vocabulaire}

\begin{methode*2*2}[Tracer une courbe représentative \MethodeRefExercice*{2F1_E_tracer_courbe}]
 \exercice
 \label{2F1_M_tracer_courbe}
  Tracer la parabole\\ représentant la fonction $f$ définie par
  $f(x)=x^2$.
  \correction
  On établit un tableau de valeurs de la fonction $f$, on reporte les coordonnées des points dans un repère puis on les relie à la main.
  \par
  \centering
   \begin{small}
    \begin{tableau}{0.95\linewidth}{8}
      \hline
      $x$    & $-3$ &$-2$& $-1$ & $0$ & $1$ & $2$ & $3$ \\\hline
      $f(x)$ & $9$  & $4$&$1$  & $0$ & $1$ & $4$ & $9$ \\\hline
    \end{tableau}
  \end{small}
\begin{tikzpicture}[general,xscale=0.6,yscale=0.3]
\draw [quadrillage] (-5,-2) grid (5,10);
\axeX{-5}{5}{-4,-2,2,4}
\axeY{-2}{10}{2,4,6, 8}
\origine
\draw [domain=-3.162:3.162,  color=F1, epais, smooth] plot(\x,{(--\x*\x)});
\foreach \x/\y in {-3/9,-2/4,0/0,1/1,-1/1,2/4,3/9}
{\draw[color=C1] (\x,\y) node{{\boldmath $+$}};}
\end{tikzpicture}
  \exercice
  Tracer l'hyperbole\\ représentant la fonction $g$ définie par
  $g(x)=\dfrac{1}{x}$.
  \correction
  \par\centering
  \begin{small}
    \begin{tableau}{\linewidth}{8}
      \hline
      $x$    & $-2$   & $-1$ & $-0{,}5$ & $0$ & $0{,}5$ & $1$ & $2$
      \\\hline
      $f(x)$ & $-0{,}5$ & $-1$ & $-2$   & \tabX & $2$   & $1$ & $0{,}5$
      \\\hline
    \end{tableau}
  \end{small}
 \begin{tikzpicture}[general,xscale=0.5,yscale=0.3]
  \draw [quadrillage] (-5,-8) grid (5,8);
\axeX{-5}{5}{-4,-2,2,4}
\axeY{-8}{8}{-2,-4,-6,2,4,6}
\origine
\draw [domain=-5:-0.125,  color=F1, epais, smooth] plot(\x,{(--1/\x)});
\draw [domain=0.125:5,  color=F1, epais, smooth] plot(\x,{(--1/\x)});
\foreach \x/\y in {-2/-0.5,-1/-1,-0.5/-2,2/0.5,1/1,0.5/2}
{\draw[color=C1] (\x,\y) node{{\boldmath  $+$}};}
\end{tikzpicture}
 \end{methode*2*2}



%3p84
\section{Détermination d'images et d'antécédents}


\begin{methode}[Calculer des images à partir d'une expression littérale \MethodeRefExercice*{2F1_E_calculer_image}]
  \exercice\label{2F1_M_calculer_image}

  Soit $f$ la fonction définie sur $\mathbb{R}$ par
  $f(x)=4x^5+6x^2-9$.

  Calculer les images de $0$ et de $-1$ par la fonction $f$.

  \correction

  $0$ et $-1$ appartiennent à $\mathbb{R}$, l'ensemble de
  définition de $f$. 
 
 $f(0) = 4\times 0^5+6\times 0^2-9 = -9$ \\
 $f(-1) = 4\times (-1)^5+6\times (-1)^2-9 = -4+ 6-9=-7  $
 
 Les images de $0$ et $-1$ par $f$ sont respectivement $-9$ et $-7$.

\end{methode}


\begin{methode}[Rechercher un (ou des) antécédent(s) par le calcul\MethodeRefExercice*{2F1_E_calcul_antecedent}]
\exercice\label{2F1_M_calcul_antecedent}

Soit $f$ la fonction définie sur $\mathbb{R}$ par $f(x)=3x-5$.

Déterminer le(s) antécédent(s) éventuel(s) de 16 par la fonction $f$.

\correction

On note $x$ un nombre dont l'image  est 16. 

$x$ est solution de l'équation $f(x)=16$ soit $3x-5=16$. 

$3x=16+5=21$ donc $x=21\div 3=7$

$7 \in \mathbb{R}$, l'ensemble de définition de $f$.

Donc,  7 est l’unique antécédent de 16 par la fonction $f$. 
\end{methode}

\begin{remarques}
\begin{itemize}
\item Calculer l'\textcolor{H1}{\textbf{image}} d'un nombre, c'est \textcolor{H1}{\textbf{évaluer}} la valeur d'une expression littérale.
\item Calculer le (ou les) \textcolor{H1}{\textbf{antécédent(s)}} d'un nombre, c'est \textcolor{H1}{\textbf{résoudre}} une équation.
\end{itemize}

\end{remarques}




\begin{methode*1}[Lire graphiquement une image et des antécédents \MethodeRefExercice*{2F1_E_lire_graphique}]

\exercice\label{2F1_M_lire_graphique}

\begin{multicols}{2}
Voici la courbe représentative d'une fonction $f$ définie sur $[-2;4]$.  
\begin{enumerate}
 \item Quelle est l'image de 3 par $f$?
 \item Par $f$, quels sont les antécédents de:
 	\begin{itemize}
 		\item $-1$ ?
 		\item 1? 
 		\item 3?
 	\end{itemize}
 		
\end{enumerate}

\begin{center}
\begin{tikzpicture}[general,scale=0.8]
\draw [quadrillage] (-2.5,-1) grid (4.5,4);
\axeX{-2.5}{4.5}{-2,2,4}
\axeY{-1}{4}{1,2, 3}
\origine
\draw [domain=-2:4,  color=J1, tres epais, smooth] plot(\x,{(--\x*\x/(2*\x+5))});
\node at (-2,3.9) {\textcolor{J1}{$\bullet$}};
\node at (4,1.25) {\textcolor{J1}{$\bullet$}};
\end{tikzpicture}
\end{center}
\end{multicols}

\correction

\begin{multicols}{2}

\begin{center}

\begin{tikzpicture}[general, scale=0.8]
\draw [quadrillage] (-2.5,-1) grid (4.5,4);
\axeX{-2.5}{4.5}{1,3}
\axeY{-1}{4}{-1,1, 3}
\origine
\draw [domain=-2:4,  color=J1, tres epais, smooth] plot(\x,{(--\x*\x/(2*\x+5))});
\node at (-2,3.9) {\textcolor{J1}{$\bullet$}};
\node at (4,1.25) {\textcolor{J1}{$\bullet$}};
\draw[color=F1, epais, dashed] (3,-1)--(3,4);
\draw[color=F1, epais, dashed, <-] (0,0.8)--(3,0.8);
 \draw[color=magenta, epais, dashed] (-2.5,-1)--(4.5,-1);
 \draw[color=G1, epais, dashed] (-2.5,1)--(4.5,1);
 \draw[color=G1, epais, dashed, ->] (-1.5,1)--(-1.5,0);
 \draw[color=G1, epais, dashed, ->] (3.5,1)--(3.5,0);
 \draw[color=H1prime, epais, dashed] (-2.5,3)--(4.5,3);
 \draw[color=H1prime, epais, dashed, ->] (-1.86,3)--(-1.86,0);
\node[left, color=F1] at (0.07,0.75) { \footnotesize {\boldmath $0,8$}} ;
 \node[below, color=G1] at (-1.4,0.1) {\footnotesize {\boldmath $-1,5$}} ;
 \node[left, color=H1prime] at (-1.8,0.15) {\footnotesize {\boldmath $-1,9$}} ;
 \node[below, color=G1] at (3.5,0.1) {\footnotesize {\boldmath $3,5$}} ;
\end{tikzpicture}

 \end{center}
 Par lecture graphique,
\begin{enumerate}
 \item 
 \textbf{\textcolor{F1}{l'image de 3}} par $f$ est environ 0,8.
 \item 
 \begin{itemize}
  \item \textbf{\textcolor{magenta} {$-1$ n'a pas d'antécédent}} par $f$.
  \item \textbf{\textcolor{G1}{1 a deux antécédents}} par $f$ : 
  
  environ $-1,5$ et 3,5.
  \item \textbf{\textcolor{H1prime}{3 a un unique antécédent}} par $f$ : 
  
  environ $-1,9$.
 \end{itemize}
\end{enumerate}
\end{multicols}


\end{methode*1}



